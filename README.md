# Watch+Jam 2 (2019)

Generic sci-fi MUD.

The theme is **NO-CLIP**, or unexpected uses of collision. I think I will mostly ignore that theme.

## To do: Rules

* import old Alternity code into the new version
  * update the old code to my modern standards
* update and personalise the rule system
  * steps
    * plus steps should be *better*, minus steps worse? 
    * better name than "situation steps"
  * display chances as percentages
  * if we can't do some of the above, can we change the system so that rolling high is better than rolling low?

## To do: Combat

* rules
  * status effects
  * healing
  * damage / death
  * armor
  * death
  * initiative
  * cover behind object?
  * hard-coded ranges?
  * cut features
    * ranges?
      * movement between objects?
      * rooms are mapped on a line with objects and exits at points?
* weapons
  * PL6 or so?
* enemies
  * simple
  * non-human?
  
## To do: game

* basic sci fi
* enter via airlock
* fight
* explore
* repair thingie
* victory
* take inspiration from the intro adventures?

## To do: actions

* non-combat
  * equip items
  * wear clothing
  * first aid
* combat
  * aim
  * reload
  * attack / shoot / hit  
  * reload
  * cut features
    * covering / supressing fire?
    
    * single / burst / auto
    * fire / burst / auto
    * throw
    * dual weild
    * dodge
    * draw
* other ideas
  * assisting
  * crit failures
  * surprise
  * disarm
  * called shot
  * break
  * charge
  * sneak attack

## To do: other

* prebuilt characters?
    * Tech Op
    * Free Agent
    * Combat Spec

## To do: tutorial

* tutorial
    * basics taught by moving inbetween stations
    * different classes have different skill sets for each station
        * Combat spec Gunnery and untrained pilot 
        * Free spec Pilot and untrained repair
        * Tech spec Repair and untrained gunnery
    * stations
        * Action roll and diff die
        * Untrained, broad, and spec skills
        * Degrees of success
* hints 
  * Damage, dying and recovery
  * Complex checks?

## To do: PANIC MODE

* web client: don't put connect command in history
* web client: show prompt in main window
* web client: up arrow for history instead of shift-up
* reload in combat
* actual level
    * "key" puzzle to anger the bots
    * tutorial areas
    * ~~AI~~
* ~~pregen char selection zones~~
* ~~give bots equipment~~
* ~~make bots attack~~
* ~~fix web~~
* ~~hint system~~
* ~~auto cleaner?~~
* ~~mob spawners~~
    * ~~security bot~~
    * ~~recycler bots?~~
* ~~PvE combat~~
* ~~knocked out status~~
* ~~health recover over time~~
    * ~~stun~~
    * ~~wound~~
* ~~first aid~~

# worked rules changes

success is < score
amazing is < score / 4

maybe call sit die "diff die"? Don't need to change order then.

1 / 20 = 5%
+d4 = 5-20% avg 13%
+d6 = 5-30% avg 18%
+d8 = 5-40% avg 23%
+d12 = 5-60% avg 33%
+d20 = 5-100% avg 53%
+2d20 = 10-200% avg 105%
+3d20 = 15-300% avg 158%

# Evennia intro follows:

This is your game directory, set up to let you start with
your new game right away. An overview of this directory is found here:
https://github.com/evennia/evennia/wiki/Directory-Overview#the-game-directory

You can delete this readme file when you've read it and you can
re-arrange things in this game-directory to suit your own sense of
organisation (the only exception is the directory structure of the
`server/` directory, which Evennia expects). If you change the structure
you must however also edit/add to your settings file to tell Evennia
where to look for things.

Your game's main configuration file is found in
`server/conf/settings.py` (but you don't need to change it to get
started). If you just created this directory (which means you'll already
have a `virtualenv` running if you followed the default instructions),
`cd` to this directory then initialize a new database using

    evennia migrate

To start the server, stand in this directory and run

    evennia start

This will start the server, logging output to the console. Make
sure to create a superuser when asked. By default you can now connect
to your new game using a MUD client on `localhost`, port `4000`.  You can
also log into the web client by pointing a browser to
`http://localhost:4001`.

# Getting started

From here on you might want to look at one of the beginner tutorials:
http://github.com/evennia/evennia/wiki/Tutorials.

Evennia's documentation is here:
https://github.com/evennia/evennia/wiki.

Enjoy!
