from world import alternity

SKILL_LIST = {} # type: dict[str, Skill]

class Skill(object):
    def __init__(self, name, ability, parent=None):
        """Object representing a skill

        :param name: str
        :param ability: str
        :param parent: str
        """
        assert ability in alternity.ABILITIES

        self.name = name    # type: str
        self.ability = ability  # type: str
        self.children = None

        # if we have no parent, we are a broad skill
        if parent is None:
            self.parent = None
            self.children = {}
        else:
            self.parent = SKILL_LIST[parent]
            self.children = None
            self.parent.children[name] = self

        # add us to the skill list
        SKILL_LIST[name] = self

    @property
    def broad(self):
        """return true if this is a broad skill"""
        return self.parent is None


# instantiate the skills to add to master skill list
Skill("Armor Operation", "strength")
Skill("Combat Armor", "strength", "Armor Operation")

Skill("Ranged Weapon Modern", "dexterity")
Skill("Pistol", "dexterity", "Ranged Weapon Modern")
Skill("Rifle", "dexterity", "Ranged Weapon Modern")
Skill("SMG", "dexterity", "Ranged Weapon Modern")

Skill("Vehicle Operation", "dexterity")
Skill("Land Vehicle", "dexterity", "Vehicle Operation")
Skill("Space Vehicle", "dexterity", "Vehicle Operation")

Skill("Stamina", "constitution")
Skill("Endurance", "constitution", "Stamina")

Skill("Technical Science", "intelligence")
Skill("Juryrig", "intelligence", "Technical Science")
Skill("Repair", "intelligence", "Technical Science")

Skill("Knowledge", "intelligence")
Skill("Computer Operation", "intelligence", "Knowledge")
Skill("Deduce", "intelligence", "Knowledge")
Skill("First Aid", "intelligence", "Knowledge")

Skill("Medical Science", "intelligence")
Skill("Surgery", "intelligence", "Medical Science")
Skill("Treatment", "intelligence", "Medical Science")

Skill("Awareness", "will")
Skill("Intuition", "will", "Awareness")
Skill("Perception", "will", "Awareness")

Skill("Resolve", "will")
Skill("Mental Resolve", "will", "Resolve")
Skill("Physical Resolve", "will", "Resolve")