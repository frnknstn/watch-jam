# core rules concepts
import itertools
from typing import List, Tuple, Optional

from evennia.utils.logger import log_info

from world.dice import DiceSet, AlternityCheck, ComplexCheck
from typeclasses.hintengine import send_hint

# Exceptions
from world.utils import chars_in_room


class RulesError(Exception):
    """Something went wrong in the rules system"""
    pass


# definitions
SPECIES = ["human", "robot"]

PROFESSIONS = ["combat spec", "diplomat", "free agent", "tech op", "none"]
PROFESSION_ACTION_CHECK_BONUS = {"combat spec": 3, "diplomat": 1, "free agent": 2, "tech op": 1, "none": 1}

ABILITIES = ["strength", "dexterity", "constitution", "intelligence", "will", "personality"]


class WeaponClassInfo(object):
    pass
class PistolWeaponClassInfo(object):
    skill = "Pistol"
    range_penalty = (-1, +1, +3)
class RifleWeaponClassInfo(object):
    skill = "Rifle"
    range_penalty = (-1, 0, +1)
class SMGWeaponClassInfo(object):
    skill = "SMG"
    range_penalty = (-1, +1, +3)


DURABILITY_TYPES = ["stun", "wound", "mortal", "fatigue"]
IMPACT_TYPES = ["LI", "HI", "En"]
WEAPON_FIRE_MODES = ["F", "FBA", "BA"]
WEAPON_CLASSES = {
    "pistol": PistolWeaponClassInfo,
    "rifle": RifleWeaponClassInfo,
    "smg": SMGWeaponClassInfo
}

# helper classes
class DamageDice(DiceSet):
    def __init__(self, count, sides, bonus, durability_type):
        """Dice set representing damage"""
        super().__init__(count, sides, bonus)
        assert durability_type in DURABILITY_TYPES
        self.durability_type = durability_type
    
    def roll(self):
        """Returns an amount of damage and the durability type of that damage
        
        :rtype: tuple[int, str] 
        """
        return (super().roll(), self.durability_type)

    def __repr__(self):
        bonus_text = "" if not self.bonus else "%+d" % self.bonus
        durability_type_text = self.durability_type[0]
        if self.count == 1:
            return "<d%d%s%s>" % (self.sides, bonus_text, durability_type_text)
        else:
            return "<%dd%d%s%s>" % (self.count, self.sides, bonus_text, durability_type_text)

# rules functions
def do_attack(attacker, target):
    """Do an attack

    :type attacker: typeclasses.characters.Character
    :type target:  typeclasses.characters.Character
    :rtype: None
    """
    # get the active weapon
    weapon = attacker.get_equipped("weapon")
    if not weapon:
        attacker.msg("Can't attack, no weapon equipped")
        return
    
    # calculate attacker bonuses and penalties
    weapon_class_info = WEAPON_CLASSES[weapon.db.weapon_class]
    check = attacker.get_skill_check(weapon_class_info.skill)

    # adjust ammo
    ammo = weapon.db.ammo - 1
    if ammo < 0:
        weapon.reload()
        ammo = weapon.db.ammo - 1
        attacker.location.msg_contents("%s reloads %s on the fly" % (attacker, weapon))
        # reload and fire penalty
        check.mod(2, "reloaded weapon")
    weapon.db.ammo = ammo 

    # Modifiers:
    # range
    # status
    # active effects
    # weapon accuracy
    # reload
    check.mod(weapon.db.accuracy_penalty, "weapon accuracy")

    # no range yet, so assume all are at mythical shmedium range and +0 step

    # defender modifiers:
    # res mod
    # dodge
    # cover
    if target.is_mobile():
        check.mod(target.dexterity_res, "target dexterity")
    else:
        # attacking a stunned target
        check.mod(-2, "target immobilised")

    # roll to hit
    hit_check = check.roll()

    # attacker.location.msg_contents(str(hit_check))
    
    damage_strings = []

    if hit_check.is_success:
        damage, durability_type = roll_weapon_damage(weapon, hit_check)
        damage_strings.extend((
            " for ",
            damage_str(damage, durability_type, short=False)
        ))
        
        # roll armor
        target_armor = target.get_equipped_armor()
        if target_armor:
            armor_dice = target_armor.impact_dice(weapon.db.impact)
            armor_result = armor_dice.roll()
            damage_strings.append(" - %d armor" % armor_result)
        else:
            armor_result = 0

        # apply damage
        final_damage = player_combat_damage(target, damage, durability_type, armor_result, source=attacker.key)
        if final_damage:
            damage_strings.append(" = ")
            damage_strings.extend(damage_str(x, y) for x, y in final_damage)

    # results?
    strings = []
    strings.append("|c%s|n attacks |c%s|n: %s" %
        (
            attacker,
            target,
            hit_check.result_description("hit", "miss"),
        ),
    )
    strings.extend(damage_strings)

    final_message = "".join(strings)

    # final_message = hit_check.result_description("hit", "miss")

    attacker.location.msg_contents(final_message, exclude=target)
    target.msg(final_message + " |w(on you!)|n")

    send_hint(attacker, f"Attack on |c{target}|n\n{check.hint_str()}")


def roll_weapon_damage(weapon, check):
    """Roll weapon damage given a check
    :type check: AlternityCheck
    :rtype: Tuple[int, str]
    """
    # roll damage
    if check.is_ordinary:
        damage_dice = weapon.db.damages[0]
    elif check.is_good:
        damage_dice = weapon.db.damages[1]
    elif check.is_amazing:
        damage_dice = weapon.db.damages[2]
        # TODO: extra effects when you get an amazing hit
    else:
        raise RulesError("Weapon check was a success but also not? %s" % str(check))
    damage, durability_type = damage_dice.roll()
    return damage, durability_type


def player_combat_damage(target, raw_damage: int, durability_type: str, armor_result: int, source: str) -> List[Tuple[int, str]]:
    """
    Work out the final damage total and reduce a player's durability, triggering effects as needed

    return a list of all damage that was actually caused, primary or secondary or whatever

    :type target: typeclasses.characters.Character
    """

    dmg = {
        "stun": 0, "wound": 0, "mortal": 0, "fatigue": 0
    }

    # secondary damage
    if durability_type == "stun":
        pass
    elif durability_type == "wound":
        dmg["stun"] += raw_damage // 2
    elif durability_type == "mortal":
        dmg["stun"] += raw_damage // 2
        dmg["wound"] += raw_damage // 2
    elif durability_type == "fatigue":
        pass

    # apply the armor
    primary_damage = max(raw_damage - armor_result, 0)
    dmg[durability_type] += primary_damage

    # apply the total damages
    retval = []
    for dur in ("mortal", "wound", "stun", "fatigue"):
        if dmg[dur] > 0:
            target.take_damage(dmg[dur], dur, source)
            retval.append((dmg[dur], dur))

    return retval


def damage_str(damage, durability_type, short=True):
    """return a short colored string representing an amount of damage e.g. 3w or 1m
    
    :type damage: int
    :type durability_type: str
    :rtype: str
    """
    if durability_type == "stun":
        color = "|x"
    elif durability_type == "wound":
        color = ""
    elif durability_type == "mortal":
        color = "|r"
    elif durability_type == "fatigue":
        color = "|m"
    else:
        raise RulesError()

    return "%s%d%s|n" % (color, damage, durability_type[0] if short else " %s damage" % durability_type)


def roll_combat_initiative(room, combat_handler):
    """Roll initiative for an entire combat

    :type room: typeclasses.rooms.Room
    :type combat_handler: typeclasses.scripts.CombatHandler
    """
    initiative_table = [list() for x in range(5)]
    
    for char in chars_in_room(room):
        if char.db.pacifist:
            continue
        act_phases = roll_player_initiative(char)
        for phase in act_phases:
            initiative_table[phase].append(char)

    combat_handler.db.initiative_table = initiative_table
    room.msg_contents((draw_initiative_table(initiative_table, combat_handler.db.turn), {'type': 'initiative'}))
        

def roll_player_initiative(character):
    """Roll initiative for a character and return what phases they act in

    :type character: typeclasses.characters.Character
    :rtype: list[int]
    """
    target = character.action_check_score
    step = 0

    # what modifiers to action check do we have?

    # armor res mod etc
    step += character.action_penalty

    # do the roll
    action_check = AlternityCheck(target, step, trivial=True)
    
    if action_check.is_marginal:
        phase = 4
    elif action_check.is_ordinary:
        phase = 3
    elif action_check.is_good:
        phase = 2
    elif action_check.is_amazing:
        phase = 1
    else:
        raise RulesError(action_check)
    
    actions = character.action_count
    act_phases = [x for x in range(1, 5) if x >= phase and x < phase + actions]
    
    character.msg("Your |Cinitiative check|n vs. %s. You act in phase |C%s|n." % (
        action_check.percent_description(success_string="speed"),
        " & ".join(str(x) for x in act_phases)
    ))
    
    return act_phases


def draw_initiative_table(tab: List[List[str]], turn) -> str:
    """returns the initiative table"""

    def table_line(left: str, line: str, formatting: Optional[str], width: int, right: str) -> str:
        line_width = width - len(left) - len(right)
        whitespace = " " * (line_width - len(line))
        formatted_line = (formatting % line) if formatting else line
        return "".join((left, formatted_line, whitespace, right))

    heading = "Initiative Turn %d" % turn

    col_w = max(len(str(x)) for x in itertools.chain(*tab))
    col_w = max(col_w, len(heading)) + 4

    lines = [        "╒" + "═" * (col_w - 2) + "╕", 
          table_line("│", heading, "|C%s|n", col_w, "│")]
    for i in range(1, 5):
        lines.append("├" + "── |Cphase %d|n " % i + "─" * (col_w - 2 - len("── phase x ")) + "┤")
        for char in tab[i]:
            lines.append(table_line("│ ", str(char), None, col_w, " │"))

    lines.append("└" + "─" * (col_w - 2) + "┘")

    return "\n".join(lines)


def do_stun_recover_check(char):
    """character is attempting to recover some stun damage

    :type char: typeclasses.characters.Character
    """
    score, mod = char.get_skill_score("Physical Resolve")
    check = AlternityCheck(score, mod)

    damage = 0
    if check.is_ordinary:
        damage = 2
    elif check.is_good:
        damage = 4
    elif check.is_amazing:
        damage = 6
    elif check.is_critical_hit:
        damage = char.db.stun_max

    if damage:
        char.heal_damage(damage, "stun")
        char.msg("You recover |g%d|n points of stun." % damage)

    log_info("%s recovers %d stun %s" % (char, damage, check))


def do_wound_recover_check(char):
    """character is attempting to recover some stun damage

    :type char: typeclasses.characters.Character
    """
    score, mod = char.get_skill_score("Physical Resolve")
    check = AlternityCheck(score, mod, trivial=True)

    damage = 0
    if check.is_critical_miss:
        damage = 0
    elif check.is_marginal:
        damage = 1
    elif check.is_ordinary:
        damage = 2
    elif check.is_good:
        damage = 3
    elif check.is_amazing:
        damage = 4
    elif check.is_critical_hit:
        damage = 5

    if damage:
        char.heal_damage(damage, "wound")
        char.msg("You recover |g%d|n points of wound." % damage)

    log_info("%s recovers %d wound %s" % (char, damage, check))


def do_heal(char, target):
    """Character is attempting to heal target.

    Pick the most urgent action and do that.

    :type char: typeclasses.characters.Character
    :type target: typeclasses.characters.Character
    """
    # work out which skill is better
    first_aid = (0, 0)
    treatment = (0, 0)
    if "First Aid" in char.db.skills:
        first_aid = char.get_skill_score("First Aid")
    if "Treatment" in char.db.skills:
        treatment = char.get_skill_score("Treatment")

    if (first_aid[0], -first_aid[1]) > (treatment[0], -treatment[1]):
        best_skill_name = "First Aid"
        best_skill = first_aid
    else:
        best_skill_name = "Treatment"
        best_skill = treatment

    if char.has_item_by_key("trauma pack"):
        heal_tool = "trauma pack"
    else:
        heal_tool = "first aid kit"

    # pick a heal method
    additional_treatments = []
    treated = False

    # Stabilise a dying patient
    if False:
        # dying from mortal damage is not implemented
        treated = True

    # Return a knocked out patient to consciousness
    elif target.has_status("unconscious - stun"):
        treated = True
        check = AlternityCheck(*best_skill)
        heal = check.switch(2, 3, 4, 0)
        target.location.msg_contents("%s tries to revive %s using %s: %s for %d stun damage" % (
            char, target, best_skill_name, check.result_description("heal"), heal))
        target.heal_damage(heal, "stun")

    # Provide |CFirst Aid|n for a patient's wounds
    elif (target.db.wound < target.db.wound_post_first_aid) and "First Aid" in char.db.skills:
        treated = True
        check = AlternityCheck(*first_aid)
        if check.is_success:
            heal = 2 if heal_tool == "trauma pack" else 1
        else:
            heal = 0

        target.location.msg_contents("%s uses |CFirst Aid|n and %s to heal %s: %s for %d wound damage" % (
            char, heal_tool, target, check.result_description("heal"), heal))

        if heal > 0:
            target.heal_damage(heal, "wound")
            target.db.wound_post_first_aid = target.db.wound
    elif target.db.wound < target.db.wound_post_first_aid:
        additional_treatments.append(
            "%s would also benefit from |CTreatment|n from someone with the skill." % target
        )

    # Provide |CTreatment|n for a patient's wounds
    elif (target.db.wound < target.db.wound_post_treatment) and "Treatment" in char.db.skills:
        treated = True
        check = AlternityCheck(*treatment)
        if check.is_success:
            heal = 4 if heal_tool == "trauma pack" else 2
        else:
            heal = 0

        target.location.msg_contents("%s uses |CTreatment|n and %s to heal %s: %s for %d wound damage" % (
            char, heal_tool, target, check.result_description("heal"), heal))

        if heal > 0:
            target.heal_damage(heal, "wound")
            target.db.wound_post_treatment = target.db.wound
    elif target.db.wound < target.db.wound_post_treatment:
        additional_treatments.append(
            "%s would also benefit from |CTreatment|n from someone with the skill." % target
        )

    # Heal some stun damage
    elif target.db.stun < target.db.stun_max:
        treated = True
        check = AlternityCheck(*treatment)
        heal = check.switch(2, 3, 4, 0)
        target.location.msg_contents("%s attempts to heal %s: %s for %d stun damage" % (
            char, target, check.result_description("heal"), heal))
        target.heal_damage(heal, "stun")

    # nothing to do here
    else:
        pass

    if not treated:
        if target.db.mortal < target.db.mortal_max:
            char.msg("%s is beyond mere healing. %s needs surgery!" % (target, target))
        else:
            char.msg("There are no treatments you know that can help %s right now." % (target,))

    for msg in additional_treatments:
        char.msg(msg)

    # if not in combat, check for new statuses
    if not target.is_in_combat():
        target.check_durability()

