"""dice and dice rolling helpers"""
from collections import OrderedDict
from math import floor, factorial
from random import randint
from typing import Optional

from evennia.utils import evtable
from evennia.utils.logger import log_info

# standard rollers
def roll(count, sides, bonus):
    """Basic xDy + z dice

    :param count: int
    :param sides: int
    :param bonus: int
    :return: int
    """
    total = 0
    for i in range(count):
        total += max(randint(1, sides) + bonus, 0)

    return total


class DiceSet(object):
    def __init__(self, count, sides, bonus):
        """Basic xDy + z dice

        :param count: int
        :param sides: int
        :param bonus: int
        """
        self.count = count
        self.sides = sides
        self.bonus = bonus

    def roll(self):
        """
        Roll the dice set, and returns the total.

        :return: int
        """
        return roll(self.count, self.sides, self.bonus)

    def __repr__(self):
        bonus_text = "" if not self.bonus else "%+d" % self.bonus
        if self.count == 1:
            return "<d%d%c>" % (self.sides, bonus_text)
        else:
            return "<%dd%d%c>" % (self.count, self.sides, bonus_text)

STEP_DICE = [
    DiceSet(0, 0, 0),   # step 0
    DiceSet(1, 4, 0),   # step 1
    DiceSet(1, 6, 0),   # step 2
    DiceSet(1, 8, 0),   # step 3
    DiceSet(1, 12, 0),  # step 4
    DiceSet(1, 20, 0),  # step 5
    DiceSet(2, 20, 0),  # step 6
    DiceSet(3, 20, 0),  # step 7
    DiceSet(4, 20, 0),  # step 8
    DiceSet(5, 20, 0),  # step 9
    DiceSet(6, 20, 0)   # step 10
]

# Alternity
class AlternityCheck(object):
    control_dice = DiceSet(1, 20, 0)

    def __init__(self, target_value, situation_step, trivial=False):
        """Perform a new Alternity check

        :param target_value: int
        :param situation_step: int
        :param trivial: bool
        """

        # parse parameters
        if situation_step < -5:
            situation_step = -5  # PHB pg. 49
        elif situation_step > 10:
            situation_step = 10  # just plain sanity

        self.target_value = target_value
        self.situation_step = situation_step
        self.trivial = trivial  # If a test is trivial, then it will have a 'marginal' result instead of a failure

        # variables to hold the results
        self.situation_dice = STEP_DICE[abs(self.situation_step)]
        self.control_result = None
        self.situation_result = None
        self.result = None

        self.is_success = False
        self.is_failure = False
        self.is_critical_hit = False
        self.is_amazing = False
        self.is_good = False
        self.is_ordinary = False
        self.is_marginal = False
        self.is_critical_miss = False

        self.roll()

    def roll(self):
        """Perform an Alternity check"""
        target_value = self.target_value
        control_result = self.control_dice.roll()
        situation_result = self.situation_dice.roll()

        result = control_result
        if self.situation_step < 0:
            # bonus
            result -= situation_result
        elif self.situation_step > 0:
            # penalty
            result += situation_result

        if control_result != 20:
            if result > target_value:
                # failure
                if not self.trivial:
                    self.is_failure = True
                else:
                    self.is_success = True
                    self.is_marginal = True
            elif result <= (target_value // 4):
                # amazing
                self.is_success = True
                self.is_amazing = True
            elif result <= (target_value // 2):
                # good
                self.is_success = True
                self.is_good = True
            elif result <= target_value:
                # ordinary
                self.is_success = True
                self.is_ordinary = True

            # critical hit
            if self.is_success and control_result == 1:
                self.is_critical_hit = True
        else:
            # critical miss
            self.is_critical_miss = True
            if self.trivial:
                # yeah, a critical miss on a trivial test is still a success unless you specifically want otherwise
                self.is_success = True
                self.is_marginal = True
            else:
                self.is_failure = True

        # set the values on the object
        self.control_result = control_result
        self.situation_result = situation_result
        self.result = result

    @property
    def probability(self):
        return check_probability(self.target_value, self.situation_step)

    def situation_description(self):
        """return a string describing the situation step.

        For example, a +2 would result in the string:
           '+2 step penalty'
        """
        situation_step = self.situation_step
        favor = ""
        color = ""

        if situation_step > 0:
            favor = " penalty"
            color = "|r"
        elif situation_step < 0:
            favor = " bonus"
            color = "|g"

        desc = "%s%+d|n step%s" % (color, situation_step, favor)
        return desc

    def result_description(self, success_string="success", failure_string="failure"):
        """return a string describing the result.

        For example, an amazing result would return the string:
           'amazing success'
        """

        if self.is_critical_hit:
            success_string = "critical %s!" % success_string

        if self.is_critical_miss:
            desc = "|RCritical %s|n" % failure_string
        elif self.is_failure:
            desc = "|r%s|n" % failure_string
        elif self.is_marginal:
            desc = "|xmarginal|n %s" % success_string
        elif self.is_ordinary:
            desc = "|Yordinary|n %s" % success_string
        elif self.is_good:
            desc = "|ygood|n %s" % success_string
        elif self.is_amazing:
            desc = "|gamazing|n %s" % success_string
        else:
            desc = "unknown??!"

        return desc

    def roll_description(self):
        """return a string representing the component dice of the result

        For example, a control die of 14 and a situation die of -6 would return:
          '14 - 6 = 8'
        """

        if self.situation_step < 0:
            sign = "-"
        else:
            sign = "+"

        desc = "%d %s %d = %d" % (self.control_result, sign, self.situation_result, self.result)
        return desc
    
    def percent_description(self, success_string="success", failure_string="failure"):
        """result description string including a percentage, e.g. "(74%): ordinary success"""
        return "%d%%: %s" % (
            self.probability,           # e.g. "84%"
            self.result_description(success_string=success_string,
                                    failure_string=failure_string),  # e.g. "ordinary success"
        )

    def percent_range_description(self):
        """result description string including a percentage, e.g. "(74%): ordinary success"""
        return "(%d%%): %s" % (
            self.probability,           # e.g. "84%"
            self.result_description(),  # e.g. "ordinary success"
        )

    def switch(self, o, g, a, f, c=None, cf=None, m=None):
        """return different values based on the degree of success

        Remember that the arguments get evaluated before they are passed to this function,
        so be careful of side effects and wasted cycles!
        """
        if self.is_critical_hit and c is not None:
            return c
        elif self.is_critical_miss and cf is None:
            return cf
        elif self.is_ordinary:
            return o
        elif self.is_failure:
            return f
        elif self.is_good:
            return g
        elif self.is_amazing:
            return a
        elif self.is_marginal:
            return m if m is not None else o
        else:
            raise Exception("unreachable switch")

    def __repr__(self):
        return "<check target %d at %s: %s (%s)>" % (
            self.target_value,
            self.situation_description(),   # e.g.  "+1 step penalty"
            self.result_description(),      # e.g. "ordinary success"
            self.roll_description()         # e.g. "(8 + 3 = 11)"
        )


class ComplexCheck(object):
    def __init__(self, target=None, base_step=0, base_step_reason="base"):
        """Build a complex Alternity check piece by piece, with hooks for hint system"""
        self.situations = OrderedDict()    # type: OrderedDict[str, int]
        self.step = 0
        self.target = target
        self.mod(base_step, base_step_reason)
        self.check = None   # type: AlternityCheck

    def mod(self, step: int, reason: str = "base"):
        """Add an additional step modifier to this check."""
        self.situations[reason] = self.situations.get(reason, 0) + step
        self.step += step

    def roll(self, target: Optional[int] = None) -> AlternityCheck:
        assert self.check is None
        if target is not None:
            assert self.target is not None    
            self.target = target
        self.check = AlternityCheck(self.target, self.step)
        return self.check
  
    def hint_str(self) -> str:
        assert self.check is not None
        check = self.check
        
        o = 100
        g = o + (self.target - (self.target // 2)) * 5
        a = o + (self.target - (self.target // 4)) * 5

        x, y, z = (
            self.target * 5,
            (20 - check.control_result) * 5,
            5 * (check.situation_result if check.situation_step <= 0 else (0 - check.situation_result)),
        )
        total = sum((x, y, z))
        total_color = check.switch("|Y", "|y", "|g", f="|r", cf="|R")

        retval = []
        retval.append("  |530<<< %s |530>>>|n" % check.result_description())
        retval.append("Target: |Y100%%|n / |y%d%%|n / |g%d%%|n" % (g, a))
        retval.append("Skill + Roll + Difficulty")
        retval.append("%5d + %-4d + %d = %s%d%%|n" % (
            x, y, z, total_color, total
        ))
        
        # show the modifiers
        retval.append("|530Difficulty modifiers:|n")
        # remove zero modifiers (is this useful?)
        retval.extend(
            (" %+2d: %s" % (0 - value, key)) for key, value in self.situations.items()
        )
        
        return "\n".join(retval)


# probability tables from -5 to +10 steps
ALT_PROBABILITIES = [[0] for x in range(16)]


def check_probability(target_value, step):
    """Return the percent chance that a check with a given target and step will succeed

    :type target_value: int
    :type step: int
    :rtype: int
    """
    return ALT_PROBABILITIES[step + 5][target_value]

def step_probability(step):
    """Return the max percent that an absolute step value can affect a check

    :type step: int
    :rtype: int
    """
    step_dice = STEP_DICE[abs(step)]
    return step_dice.count * step_dice.sides * 5

def dice_sum_le(target, count, sides):
    """the probability that the sum of 'count' dice with 'sides' sides is less than or equal to a target

    Based on the formulae here:
    https://www.quora.com/Is-there-a-formula-to-calculate-the-probability-of-the-sum-of-x-dice-being-than-y
    https://www.lucamoroni.it/the-dice-roll-sum-problem/
    """

    def binomial_coefficient(n, k):
        if n == k:
            return 1
        elif k == 1:
            return n
        elif k > n:
            return 0
        else:
            a = factorial(n)
            b = factorial(k)
            c = factorial(n - k)
            div = a // (b * c)
            return div

    i = 0
    i_max = floor((target - count) / sides)
    total = 0
    while i <= i_max:
        total += (((-1) ** i) * binomial_coefficient(count, i) * binomial_coefficient(target - (sides * i), count)) / (sides ** count)
        i += 1

    return total


def calc_alt_probabilities():
    """Gracefully pre-calculate our way through populating the alt probability tables"""
    log_info("Generating probability lookup tables")
    for step in range(-5, 11):
        is_penalty = (step >= 0)
        table_index = step + 5
        table = ALT_PROBABILITIES[table_index]
        step_dice = STEP_DICE[abs(step)]

        for target in range(1, 21):
            if step_dice.sides == 20 and is_penalty:
                # the whole roll is just a adding up a pile of d20s
                prob = int(dice_sum_le(target, step_dice.count + 1, 20) * 100)
            else:
                # full calculation
                prob = 0.0
                for roll in range(1, 21):
                    if step_dice.count == 0 and roll <= target:
                        # no step dice
                        prob += (100 / 20)
                    elif is_penalty:
                        # penalty
                        step_target = target - roll
                        if step_target >= step_dice.count:
                            prob += dice_sum_le(step_target, step_dice.count, step_dice.sides) * (100 / 20)
                        else:
                            # no chance
                            pass
                    else:
                        # bonus, invert the problem
                        step_target = (-1) - (target - roll)
                        prob += (1 - dice_sum_le(step_target, step_dice.count, step_dice.sides)) * (100 / 20)
                prob = int(prob)
            table.append(prob)
    log_info(evtable.EvTable(table=ALT_PROBABILITIES, border="none"))
calc_alt_probabilities()

