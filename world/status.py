import typing

from evennia.utils.logger import log_info, log_trace
from world import alternity
import typeclasses.scripts

from evennia import TICKER_HANDLER as tickerhandler

STATUS_TICK_INTERVAL = 7   # seconds

status_tick_count = 0
status_effects_details = {
    "unconscious - stun": {
        "description": "unconscious (stun damage)",
        "description_short": "unconscious (s)",
        "details": "knocked out from taking too much stun damage",
    },
    "unconscious - wound": {
        "description": "unconscious (wound damage)",
        "description_short": "unconscious (w)",
        "details": "badly injured and unconscious out from taking too much wound damage",
    },
    "unconscious - fatigue": {
        "description": "unconscious (fatigue damage)",
        "description_short": "unconscious (f)",
        "details": "passed out from extreme fatigue damage",
    },
    "out cold": {
        "description": "out cold",
        "description_short": "out cold",
        "details": "unable to recover stun damage without medical intervention",
        "default_duration": 8,
    },
    "natural healing": {
        # This status acts as a cooldown for wound healing. The check_durability() method will
        # add the status when it sees someone is wounded, but our status check happens in between
        # the status ticking down and the check_durability() call.
        "description": "natural healing wounds",
        "description_short": "natural healing",
        "details": "natural healing is slowly repairing your wounds",
        "default_duration": 2,
    },
}
all_status_effects = set(status_effects_details.keys())

status_categories = {
    "unconscious": (
        "unconscious - stun",
        "unconscious - wound",
        "unconscious - fatigue",
    )
}
all_status_categories = set(status_categories.keys())


class StatusEffect(object):
    def __init__(self, status_type: str, duration: typing.Optional[int] = None):
        """Simple structure to hold status effect details"""
        assert status_type in all_status_effects
        assert duration != 0    # zero-tick status effects make no sense and break code

        details = status_effects_details[status_type]

        self.status_type = status_type
        self.description = details["description"]
        self.description_short = details["description_short"]
        self.details = details["details"]

        # duration in multiples of STATUS_TICK_INTERVAL
        if duration is None:
            duration = details.get("default_duration", None)
        self.duration = duration - 1 if duration else None

    def __repr__(self):
        return "<'%s'%s>" % (
            self.status_type,
            "" if self.duration is None else " for %d ticks" % self.duration
        )


class StatusTickScript(typeclasses.scripts.Script):
    def at_script_creation(self):
        """The out-of-combat status ticker"""
        self.key = "Status Tick"
        self.interval = STATUS_TICK_INTERVAL
        self.persistent = True
        self.start_delay = False

    def at_repeat(self):
        try:
            if self.obj.is_in_combat():
                return
            do_char_status_tick(self.obj)
        except:
            log_trace()


def do_char_status_tick(char):
    """Process a character's status ticks

    :type char: typeclasses.characters.Character
    """
    char.tick_down_statuses()

    # heal stun over time
    if char.db.stun < char.db.stun_max and not char.has_status("out cold"):
        alternity.do_stun_recover_check(char)

    # heal wound over time
    if not char.has_status("natural healing"):
        if char.db.wound < char.db.wound_max:
            alternity.do_wound_recover_check(char)
        else:
            char.remove_status("natural healing")

    char.check_durability()


def do_char_combat_status_tick(char):
    """Process a character's status ticks while in combat

    :type char: typeclasses.characters.Character
    """
    char.tick_down_statuses()

    # only heal stun while unconscious
    if char.has_status("unconscious - stun") and not char.has_status("out cold"):
        alternity.do_stun_recover_check(char)

    char.check_durability()

