from world.alternity import DamageDice
from world.dice import DiceSet

"""
Prototypes

A prototype is a simple way to create individualized instances of a
given `Typeclass`. For example, you might have a Sword typeclass that
implements everything a Sword would need to do. The only difference
between different individual Swords would be their key, description
and some Attributes. The Prototype system allows to create a range of
such Swords with only minor variations. Prototypes can also inherit
and combine together to form entire hierarchies (such as giving all
Sabres and all Broadswords some common properties). Note that bigger
variations, such as custom commands or functionality belong in a
hierarchy of typeclasses instead.

Example prototypes are read by the `@spawn` command but is also easily
available to use from code via `evennia.spawn` or `evennia.utils.spawner`.
Each prototype should be a dictionary. Use the same name as the
variable to refer to other prototypes.

Possible keywords are:
    prototype_parent - string pointing to parent prototype of this structure.
    key - string, the main object identifier.
    typeclass - string, if not set, will use `settings.BASE_OBJECT_TYPECLASS`.
    location - this should be a valid object or #dbref.
    home - valid object or #dbref.
    destination - only valid for exits (object or dbref).

    permissions - string or list of permission strings.
    locks - a lock-string.
    aliases - string or list of strings.

    ndb_<name> - value of a nattribute (the "ndb_" part is ignored).
    any other keywords are interpreted as Attributes and their values.

See the `@spawn` command and `evennia.utils.spawner` for more info.

"""

# from random import randint
#
# GOBLIN = {
# "key": "goblin grunt",
# "health": lambda: randint(20,30),
# "resists": ["cold", "poison"],
# "attacks": ["fists"],
# "weaknesses": ["fire", "light"]
# }
#
# GOBLIN_WIZARD = {
# "prototype_parent": "GOBLIN",
# "key": "goblin wizard",
# "spells": ["fire ball", "lighting bolt"]
# }
#
# GOBLIN_ARCHER = {
# "prototype_parent": "GOBLIN",
# "key": "goblin archer",
# "attacks": ["short bow"]
# }
#
# This is an example of a prototype without a prototype
# (nor key) of its own, so it should normally only be
# used as a mix-in, as in the example of the goblin
# archwizard below.
# ARCHWIZARD_MIXIN = {
# "attacks": ["archwizard staff"],
# "spells": ["greater fire ball", "greater lighting"]
# }
#
# GOBLIN_ARCHWIZARD = {
# "key": "goblin archwizard",
# "prototype_parent" : ("GOBLIN_WIZARD", "ARCHWIZARD_MIXIN")
# }

BASE_WEAPON = {
    "typeclass": "typeclasses.objects.Weapon",
}

BASE_PISTOL = {
    "prototype_parent": "BASE_WEAPON",
    "weapon_class": "pistol",
    "impact": "HI",
    "fire_modes": "F"
}

BASE_RIFLE = {
    "prototype_parent": "BASE_WEAPON",
    "weapon_class": "rifle",
    "impact": "HI",
    "fire_modes": "FBA"
}

BASE_SMG = {
    "prototype_parent": "BASE_WEAPON",
    "weapon_class": "smg",
    "impact": "HI",
    "fire_modes": "BA"
}

PISTOL_11MM_CHARGE = {
    "prototype_parent": "BASE_PISTOL",
    "key": "11mm charge pistol",
    "damages": (
        DamageDice(1, 4, 2, "wound"),
        DamageDice(1, 6, 2, "wound"),
        DamageDice(1, 4, 1, "mortal")
    ),
    "ranges": (10, 20, 80),
    "clip_size": 15,
    "ammo": 15,
    "ammo_type": "11mm",
}

LASER_PISTOL = {
    "prototype_parent": "BASE_PISTOL",
    "key": "laser pistol",
    "damages": (
        DamageDice(1, 4, 1, "wound"),
        DamageDice(1, 6, 1, "wound"),
        DamageDice(1, 4, 0, "mortal")
    ),
    "ranges": (20, 40, 200),
    "clip_size": 20,
    "ammo": 20,
    "ammo_type": "laser",
    "accuracy_penalty": -1,
}

RENDER_RIFLE = {
    "prototype_parent": "BASE_RIFLE",
    "key": "render rifle",
    "damages": (
        DamageDice(1, 6, 1, "stun"),
        DamageDice(1, 6, 1, "wound"),
        DamageDice(1, 4, 1, "mortal")
    ),
    "ranges": (50, 100, 250),
    "clip_size": 8,
    "ammo": 8,
    "ammo_type": "render",
    "fire_modes": "F"
}

BASE_ARMOR = {
    "typeclass": "typeclasses.objects.Armor",
}

BATTLE_VEST = {
    "prototype_parent": "BASE_ARMOR",
    "key": "battle vest",
    "damages": (
        DiceSet(1, 6, -3), # LI
        DiceSet(1, 6, -2), # HI
        DiceSet(1, 4, -2)  # Energy
    ),
    "hide": +2,
    "skill": None,
    "action_penalty": 0
}

BATTLE_JACKET = {
    "prototype_parent": "BASE_ARMOR",
    "key": "battle jacket",
    "damages": (
        DiceSet(1, 6, -1),  # LI
        DiceSet(1, 4, +1),  # HI
        DiceSet(1, 4, -1)  # Energy
    ),
    "hide": +1,
    "skill": "Armor Operation",
    "action_penalty": +1
}

CARBON_FIBER_COAT = {
    "prototype_parent": "BASE_ARMOR",
    "key": "carbon fiber coat",
    "aliases": ["carbon fibre coat", "cf coat"],
    "damages": (
        DiceSet(1, 4, -1),  # LI
        DiceSet(1, 4, -1),  # HI
        DiceSet(1, 6, -3)  # Energy
    ),
    "hide": +3,
    "skill": None,
    "action_penalty": 0
}

GRAPHITE_FIBER_COAT = {
    "prototype_parent": "BASE_ARMOR",
    "key": "graphite fiber coat",
    "aliases": ["graphite fibre coat"],
    "damages": (
        DiceSet(1, 4, 0),  # LI
        DiceSet(1, 4, -1),  # HI
        DiceSet(1, 6, -1)  # Energy
    ),
    "hide": +3,
    "skill": None,
    "action_penalty": 0
}

BASE_MOB = {
    "typeclass": "typeclasses.mobs.Mob"
}

SECURITY_BOT = {
    "prototype_parent": "BASE_MOB",
    "key": "security bot",
    "aliases": ["robot"],
    "desc": "A bipedal robot frame with a grubby finish. It looks like a thin metal skeleton "
            "with no head, and with a plastic case where its ribs should be. It has two rudimentary "
            "arms capable of holding a normal firearm."
}

CLEANER_BOT = {
    "prototype_parent": "BASE_MOB",
    "typeclass": "typeclasses.mobs.CleanerMob",
    "key": "recycling bot",
    "aliases": ["recycler", "cleaner", "robot"],
    "desc": "This recycling bot is dome-shaped, about a meter in diameter and "
            "half a meter tall. It scurries along close to the ground, "
            "pausing briefly to prod anything it finds with its sensor stick. It has a "
            "cutting torch on an folded arm near its summit. Someone has gone to "
            "great care to paint this robot in the station's color scheme.",
    "spawn_msg": "A small hatch opens in the wall and a recycling bot scurries into the room.",
}

MEDICAL_KIT_DESC = ""
"If you have either of the skills |CKnowledge - First Aid|n or |Medical Science - "
"Treatment|n you can use this kit with the |cheal|n command. The skill can stabilise a"
"dying patient, rouse an unconscious patient, heal some wounds or shrug off some stun "
"damage"

FIRST_AID_KIT = {
    "typeclass": "typeclasses.objects.AltObject",
    "key": "first aid kit",
    "desc_short": "A first aid kit in a square tin",
    "desc": "A white square metal tin about 30cm wide and 5cm deep. There is a red cross on the "
            "lid of the tin. Inside there are various stabilising drugs and medical paraphinalia. "
            "\n" + MEDICAL_KIT_DESC
}

TRAUMA_PACK = {
    "typeclass": "typeclasses.objects.AltObject",
    "key": "trauma pack",
    "desc_short": "A portable trauma pack with high-tech equipment and chems",
    "desc": "A grey metal case containing high-tech portable medical equipment and chems. The case "
            "is as wide as a briefcase and about half as long. It has a carry handle and pegs for "
            "attaching it to the gear of a combat medic."
            "\n" + MEDICAL_KIT_DESC
}
