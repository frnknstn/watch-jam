"""Helper functions"""

from typing import List, Optional

from evennia import DefaultCharacter


# helper funcs
def clamp(min_val, val, max_val):
    """Change val to enforce min_val <= val <= max_val"""
    if val <= min_val:
        return min_val
    elif val >= max_val:
        return max_val
    else:
        return val


def chars_in_room(room, exclude: Optional[DefaultCharacter] = None, species: Optional[str] = None) -> List:
    """What is the right way to iterate through characters? I don't know, so I put all of my ignorance into one function

    :type room: typeclasses.rooms.Room
    """
    # todo: speciest
    if not exclude:
        base = (x for x in room.contents if isinstance(x, DefaultCharacter))
    else:
        base = (x for x in room.contents if isinstance(x, DefaultCharacter) and x != exclude)
    
    if not species:
        return list(base)
    else:
        return [x for x in base if x.db.species == species]


def rg_grad(val):
    """Returns an xterm256 color on a red->green spectrum for a given fraction [0.0, 1.0]

    This unfortunately may not be colourblind friendly...
    """
    val = round(val * 10)
    if val >= 5:
        g = 5
        r = 10 - val
    else:
        r = 5
        g = val
    return "|%d%d0" % (r, g)
