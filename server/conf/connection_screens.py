# -*- coding: utf-8 -*-
"""
Connection screen

This is the text to show the user when they first connect to the game (before
they log in).

To change the login screen in this module, do one of the following:

- Define a function `connection_screen()`, taking no arguments. This will be
  called first and must return the full string to act as the connection screen.
  This can be used to produce more dynamic screens.
- Alternatively, define a string variable in the outermost scope of this module
  with the connection string that should be displayed. If more than one such
  variable is given, Evennia will pick one of them at random.

The commands available to the user when the connection screen is shown
are defined in evennia.default_cmds.UnloggedinCmdSet. The parsing and display
of the screen is done by the unlogged-in "look" command.

"""

from django.conf import settings
from evennia import utils
from evennia import Command
import evennia.commands.default.unloggedin


def connection_screen():
    return CONNECTION_SCREEN_TEXT


CONNECTION_SCREEN_TEXT = """
|C================================================================|n
 Welcome to |530{}|n, version |w{}|n!
 
 A bland, buggy, feature-lite military shooter hacked together 
 from code examples and free assets. 
 
 Vote for this game on |gSteam Greenlight!|n

 To join, just pick a name and password for yourself and type:
      |wcreate "username" "password"|n

 When you come back later, you can connect by typing:
      |wconnect "username" "password"|n
 
 If you have spaces in your username, enclose it in quotes.
 Enter |whelp|n for more info. |wlook|n will re-show this screen.
 
 If you'd like to connect to the MUD using a 3rd-party telnet 
 client, type |wtelnet|n for details
|C================================================================|n""" \
    .format(settings.SERVERNAME, settings.GAME_VERSION)


class CmdTelnet(Command):
    key = "telnet"

    def func(self):
        string = """
If you want to use a 3rd party You can connect to this server on ports |530%s|n. I don't have much \
experience with modern MUD clients, but I did get Mudlet to work and it is fine. Here are the tweaks \
and settings I used:

    |CGeneral |n->|c Server data encoding:|n UTF-8
    |CSpecial Options |n->|c Force MXP negotiation off:|n ✓ |x(enabled the option)|n
    |CInput line |n->|c Command separator:|n ;;
        |x(Some of the build command in the MUD engine use single ;)|n

Additionally, if you can't get your client to send the messages tagged "hints" to a separate window, \
you can toggle the hint system once you are logged in by typing |chints|n.
""" % str(settings.TELNET_PORTS)

        self.caller.msg(string)


class CmdUnconnectedHelp(evennia.commands.default.unloggedin.CmdUnconnectedHelp):
    def func(self):
        """Add more stuff to the default help"""
        super().func()

        string = """\
Other commands:

  |wtelnet|n - info on using 3rd-party MUD telnet clients
"""
        self.caller.msg(string)


class CmdHintTooSoon(Command):
    key = "hints"
    aliases = ["hint"]

    def func(self):
        string = """\
|530Hint:|n You must first connect to the MUD before you can toggle hints :)
Type |Wlook|n to show the welcome screen to see how to do that.
"""
        self.caller.msg(string)