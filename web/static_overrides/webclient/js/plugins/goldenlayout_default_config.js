/*
 * Define the default GoldenLayout-based config
 *
 * The layout defined here will need to be customized based on which plugins
 * you are using and what layout you want players to see by default.
 *
 * This needs to be loaded in the HTML before the goldenlayout.js plugin
 *
 * The contents of the global variable will be overwritten by what is in the
 * browser's localstorage after visiting this site.
 *
 * For full documentation on all of the keywords see:
 *         http://golden-layout.com/docs/Config.html
 *
 */
var goldenlayout_config = { // Global Variable used in goldenlayout.js init()
    content: [
        {
            type: "row",
            content: [
                {
                    type: "column",
                    width: 70,
                    content: [
                        {
                            type: "component",
                            componentName: "Main",
                            title: "Watch+Jam 2",
                            isClosable: false,
                            tooltip: "Main - drag to desired position.",
                            componentState: {
                                types: "untagged",
                                updateMethod: "newlines",
                            }
                        },
                        {
                            type: "component",
                            componentName: "input",
                            id: "inputComponent", // mark for ignore
                            height: 12,  // percentage
                            isClosable: false,
                            tooltip: "Input - The last input in the layout is always the default.",
                        }
                    ]
                }, {
                    type: "column",
                    width: 30,
                    content: [
                        {
                            type: "component",
                            componentName: "evennia",
                            title: "initiative",
                            height:60,  // percentage
                            isClosable: false,
                            tooltip: "drag to desired position.",
                            componentState: {
                                types: "initiative",
                                updateMethod: "replace",
                            }
                        },
                        {
                            type: "component",
                            componentName: "evennia",
                            title: "hints",
                            height: 40, // percentage
                            isClosable: false,
                            tooltip: "drag to desired position.",
                            componentState: {
                                types: "hints",
                                updateMethod: "replace"
                            },
                        }
                    ]
        
                }
            ]
        }
    ]
};
