/*
 *
 * Evennia Webclient new text marker
 *
 * Draw a line to separate old messages from the new text from after your last input
 * 
 */
let newtextmarker_plugin = (function () {
    // the most-recent message at the time of our last input     
    var markedMessage = undefined;

    //
    // clear the current position of our marker 
    var clearMarker = function () {
        if (markedMessage) {
            markedMessage.classList.remove("lastmessage");
        }
    };

    //
    // clear the current position of our marker 
    var setMarker = function () {
        let mwin = $("#messagewindow");
        markedMessage = mwin.children(".out").last()[0];
        if (markedMessage) {
            markedMessage.classList.add("lastmessage");
        };
    };
    
    //
    // listen for onSend events to update the marker position 
    var onSend = function (line) {
        clearMarker();
        setMarker();
        return null;
    };
    
    //
    // Mandatory plugin init function
    var init = function () {
        // add our required styles to the DOM
        let style = document.createElement('style');
        style.innerHTML = `
            .lastmessage {
                position: relative;
            }
            .lastmessage::after {
                border-top: 1px solid;
                width: 100%;
                height: 0%;
                opacity: 70%;
                position: absolute;
                bottom: 0;
                left: 0;
                content: '';
            }
        `;
        document.head.appendChild(style);
        console.log('New text marker plugin initialized');
    };
        
    return {
        init: init,
        onSend: onSend,
    }
}());
window.plugin_handler.add("newtextmarker_plugin", newtextmarker_plugin);
