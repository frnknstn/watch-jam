from evennia import search_object, DefaultScript
from evennia.utils.logger import log_info, log_trace
from typeclasses.accounts import Account


class HintEngine(DefaultScript):
    """Global singleton script to provide characters with helpful hints while playing"""

    sorted_hints = [
"""Welcome to |CWatch|n+|520Jam|n MUD 2: 
|W"Something something Space something Malfunction!'|n

At any time you can type |yHelp|n to see a list of available commands.

To |520repeat|n your last command, press |yShift|n + |yUp|n arrow.      

To turn hints off, type |520hints|n.
""",
"""Here are some commands to get you started:

|ylook|n or |yl|n - look at your surroundings
|ylook |Y<something>|n - get more details for something you see in the room.

|ynorth|n, |ysouth|n, |yeast|n, |ywest|n - Move to a new room in that direction. The look command will normally tell you what exits are available. You can also type |yn|n, |ys|n, |ye|n, |yw|n to move.     
""", """Here are some more more basic commands:

|ychar|n - see your character info sheet.

|yinv|n or |yi|n - take inventory what you are carrying.

|ysay |Y<words>|n - say something out loud for others to hear
""", """Basic combat commands:

|yequip|n and |yunequip|n - chose what combat equipment you are using.

|yattack|n |Y<someone>|n - start combat by attacking someone, or change the target you are attacking 

|yreload|n - reload your equipped weapon next turn instead of attacking
""", """Fun fact of the day: Although there is some disagreement about what to call the thing you |wclip|n into the gun that holds the ammo, one thing is certain: the correct term is |rNOT|Y "magazine"|n.
  
  A magazine is a building used to house artillery ammunition. That term has been in use for at least 300 years, so stand against those devils who seek to redefine the meanings of words!
""", """|YDifficulty roll scaling:|n
Having some penalties is fine but they can soon add up. Maybe find ways to improve the situation!
  Each net level of |Cdifficulty|n increases the maximum penalty you can get from your difficulty roll.
       Similarly a net level of |Cease|n
     |520▌|n gives a bonus roll up to the 
     |520▌|n same max value: 
    |421▌|520▌|n  1=|02220%|n 2=|12230%|n 3=|22140%|n 4=|42160%|n
  |122▖|221▌|421▌|520▌|n  5=|520100%|n 6+=an additional
 |022▌|122▌|221▌|421▌|520▌|n               |520100%|n per!
-|0221|1222|2213|4214|5205|n
""",
# """|YDifficulty roll scaling:|n
# A low difficulty penalty is fine but they can soon add up. Maybe find ways to improve the situation!
#   Each net level of |Cdifficulty|n increases the maximum penalty you can get on your difficulty roll.
#        Similarly, a net level of |Cease|n
#      ▌ gives a bonus roll up to the
#      ▌ same max value:
#     ▌▌  1=20% 2=30% 3=40% 4=60%
#   ▖▌▌▌  5=100% 6+=an additional
#  ▌▌▌▌▌               100% per!
# -|0221|1222|2213|4214|5205|n difficulty / ease
# """,
"""Stun damage:

Stun damage represents getting bruised, scraped and battered. Take too much and you will get knocked unconscious for a while.

After a while you will wake up by yourself if you pass a |CPhysical Resolve|n check. Outside of combat you will also heal stun damage over time. 
""",
]

    def at_script_creation(self):
        self.key = "Hint Engine"
        self.interval = 40  # seconds
        self.persistent = True
        self.start_delay = True

        self.db.hint_count = 0

    def at_repeat(self):
        """Find payers, sent hints"""
        try:
            all_accounts = Account.objects.all()
            all_players = (x.character for x in all_accounts
                           if x.character
                           and x.character.db.hints_enabled)

            for player in all_players:
                
                hint_index = self.db.hint_count % len(self.sorted_hints)
                hint = self.sorted_hints[hint_index]
                send_hint(player, hint)

            if all_players:
                self.db.hint_count += 1

        except:
            log_trace()


def send_hint(player, hint: str):
    """
    
    :type player: typeclasses.characters.Character
    """
    player.msg(("|530Hint|n: " + hint, {"type": "hints"}))
