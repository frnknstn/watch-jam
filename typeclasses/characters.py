"""
Characters

Characters are (by default) Objects setup to be puppeted by Accounts.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
import typing
from typing import Any, Tuple, Optional

import math
import random
from math import ceil

import evennia
from evennia import DefaultCharacter
from evennia.utils.logger import log_info
import evennia.prototypes.spawner
from evennia.utils.search import search_object

from world import alternity, prototypes
from world.alternity import PROFESSIONS, PROFESSION_ACTION_CHECK_BONUS, DURABILITY_TYPES
from world.dice import roll, ComplexCheck
from world.skills import SKILL_LIST
from world.status import StatusEffect, all_status_effects, all_status_categories, status_categories, StatusTickScript

import typeclasses.objects

from commands.default_cmdsets import SkillCmdSet, CombatCmdSet
from world.utils import rg_grad

pregen_skills = {
    "free agent": {
        "Ranged Weapon Modern": 1, "Pistol": 1,
        "Knowledge": 1, "First Aid": 1,
        "Vehicle Operation": 1, "Space Vehicle": 1,
        "Resolve": 1, "Physical Resolve": 1,
        "Technical Science": 1,
    },
    "combat spec": {
        "Ranged Weapon Modern": 1, "Pistol": 1, "Rifle": 3,
        "Stamina": 1, "Endurance": 1,
        "Resolve": 1,
        "Technical Science": 1,
    },
    "tech op": {
        "Ranged Weapon Modern": 1, "Pistol": 2,
        "Knowledge": 1, "First Aid": 1,
        "Medical Science": 1, "Treatment": 1,
        "Technical Science": 1, "Repair": 1,
    },
}

pregen_items = {
    "free agent": [
        prototypes.PISTOL_11MM_CHARGE,
        prototypes.CARBON_FIBER_COAT,
        prototypes.FIRST_AID_KIT,
    ],
    "combat spec": [
        prototypes.RENDER_RIFLE,
        prototypes.BATTLE_VEST,
    ],
    "tech op": [
        prototypes.PISTOL_11MM_CHARGE,
        prototypes.CARBON_FIBER_COAT,
        prototypes.TRAUMA_PACK,
    ],
}


class Character(DefaultCharacter):
    """
    The Character defaults to reimplementing some of base Object's hook methods with the
    following functionality:

    at_basetype_setup - always assigns the DefaultCmdSet to this object type
                    (important!)sets locks so character cannot be picked up
                    and its commands only be called by itself, not anyone else.
                    (to change things, use at_object_creation() instead).
    at_after_move(source_location) - Launches the "look" command after every move.
    at_post_unpuppet(account) -  when Account disconnects from the Character, we
                    store the current location in the pre_logout_location Attribute and
                    move it to a None-location so the "unpuppeted" character
                    object does not need to stay on grid. Echoes "Account has disconnected"
                    to the room.
    at_pre_puppet - Just before Account re-connects, retrieves the character's
                    pre_logout_location Attribute and move it back on the grid.
    at_post_puppet - Echoes "AccountName has entered the game" to the room.

    """

    def at_object_creation(self):
        self.go_to_start_location()

        # commands
        for item in (SkillCmdSet, CombatCmdSet):
            while self.cmdset.has(item):
                log_info("Removing existing command set %s" % item)
                self.cmdset.remove(item)
            self.cmdset.add(item, permanent=True)

        # basic Alternity ideas
        self.db.profession = None
        self.db.species = "human"

        # abilities
        self.db.strength = roll(2, 6, +2)
        self.db.dexterity = roll(2, 6, +2)
        self.db.constitution = roll(2, 6, +2)
        self.db.intelligence = roll(2, 6, +2)
        self.db.will = roll(2, 6, +2)
        self.db.personality = roll(2, 6, +2)

        # durability
        self.db.stun_max = self.db.constitution
        self.db.wound_max = self.db.constitution
        self.db.mortal_max = ceil(self.db.constitution / 2)
        self.db.fatigue_max = ceil(self.db.constitution / 2)

        self.db.stun = self.db.stun_max
        self.db.wound = self.db.wound_max
        self.db.mortal = self.db.mortal_max
        self.db.fatigue = self.db.fatigue_max

        # track what wound points are eligible for heal skills
        self.db.wound_post_first_aid = self.db.wound_max
        self.db.wound_post_treatment = self.db.wound_max

        # skills
        self.db.skills = []

        # equipment
        if not self.db.equipment:
            self.db.equipment = []

        # combat
        self.db.combat_handler = None
        self.db.combat_action = None
        self.db.combat_action_description = ""
        self.db.respawn_point = self.home

        # status effects
        self.db.status_effects = {}  # type: dict[str: StatusEffect]

        old_scripts = self.scripts.get("Status Tick")
        for old_script in old_scripts:
            old_script.stop()
        self.scripts.add(StatusTickScript)

        # random stats
        self.db.respawn_count = 0
        self.db.hints_enabled = True

    def pick_profession(self, profession: str):
        """set our profession, skills and gear"""
        assert profession in PROFESSIONS

        old_profession = self.db.profession
        self.db.profession = profession
        self.db.skills = pregen_skills[self.db.profession]

        if old_profession is None:
            self.make_spawn_equipment()

        self.msg("You are now a |530%s|n!" % profession)

    def go_to_start_location(self):
        # the settings.START_LOCATION doesn't work with MULTISESSION_MODE < 2 and
        # needs a dbid instead of a key anyway
        start_locations = [
            x for x in search_object(evennia.settings.START_LOCATION, exact=True)
            if isinstance(x, evennia.DefaultRoom)
        ]
        if len(start_locations) == 1:
            self.location = start_locations[0]
            log_info("Dynamically assigning character start location %s to %s" %(
                evennia.settings.START_LOCATION, self.location
            ))


    def get_ability_score(self, ability_name):
        """Get our ability score for an ability specified by a string

        :type ability_name: str
        """
        assert ability_name in alternity.ABILITIES
        return getattr(self.db, ability_name)

    def get_skill_score(self, skill) -> Tuple[int, int]:
        """Determine the effective skill score and difficulty modifier

        Returns (score, situational modifier)
        """
        score, mod, _ = self.get_detailed_skill_score(skill)
        return (score, mod)
    
    def get_skill_check(self, skill) -> ComplexCheck:
        """Return a pre-populated skill check helper
        
        :type skill: Skill or str
        """
        target, base_mod, base_reason = self.get_detailed_skill_score(skill)
        return ComplexCheck(target, base_mod, base_reason)
    
    def get_detailed_skill_score(self, skill) -> Tuple[int, int, str]:
        """Determine the effective skill score, the difficulty modifier, and the reason for the mod 

        Returns (score, situational modifier, mod reason)
        """
        if isinstance(skill, str):
            skill = SKILL_LIST[skill]

        if skill.broad:
            # broad skill
            if skill.name in self.db.skills:
                return (self.get_ability_score(skill.ability), +1, "broad skill")
            else:
                # untrained
                return (self.get_ability_score(skill.ability) // 2, +1, "untrained broad skill")
        else:
            # speciality skill
            if skill.name in self.db.skills:
                # trained
                return (
                    self.get_ability_score(skill.ability) + self.db.skills[skill.name], 0, "trained"
                )
            elif skill.parent.name in self.db.skills:
                # broad skill only
                return (self.get_ability_score(skill.ability), +1, "broad skill only")
            else:
                # untrained
                return (self.get_ability_score(skill.ability) // 2, +1, "untrained")

    # resistance modifiers
    @property
    def strength_res(self): return self.calc_resistance_modifier(self.db.strength)
    @property
    def dexterity_res(self): return self.calc_resistance_modifier(self.db.dexterity)
    @property
    def constitution_res(self): return self.calc_resistance_modifier(self.db.constitution)
    @property
    def intelligence_res(self): return self.calc_resistance_modifier(self.db.intelligence)
    @property
    def will_res(self): return self.calc_resistance_modifier(self.db.will)
    @property
    def personality_res(self): return self.calc_resistance_modifier(self.db.personality)

    @staticmethod
    def calc_resistance_modifier(score):
        """Calculate the base resistance modifier for an ability score

        :type score: int
        :returns: int
        """
        if score >=7 and score <= 10:
            return 0
        elif score >=11 and score <= 12:
            return 1
        elif score >=13 and score <= 14:
            return 2
        elif score >=15 and score <= 16:
            return 3
        elif score >=17 and score <= 18:
            return 4
        elif score >= 19:
            return 5
        elif score >= 5 and score <= 6:
            return -1
        elif score <= 4:
            return -2

    @property
    def action_check_score(self):
        return self.calc_action_check_score(self.db.dexterity, self.db.intelligence, self.db.profession)

    @property
    def action_penalty(self):
        armor = self.get_equipped_armor()
        return armor.db.action_penalty if armor else 0
    
    @property
    def action_count(self):
        return (self.db.constitution + self.db.will) // 8

    @staticmethod
    def calc_action_check_score(dexterity, intelligence, profession):
        """Calculate a base action check score

        :param dexterity: int
        :param intelligence: int
        :param profession: string
        :return: int
        """
        return (dexterity + intelligence) // 2 + PROFESSION_ACTION_CHECK_BONUS[profession]

    def is_mobile(self):
        """Can this character currently move or dodge"""
        return not self.has_status_category("unconscious")

    def has_item_by_key(self, key: str) -> bool:
        """Inventory contains an item with a specified key"""
        return any(x for x in self.contents if x.key == key)

    # durability operations
    def is_alive(self):
        return self.db.mortal > 0

    def heal_damage(self, damage, durability_type: str):
        """remove some damage from our total"""
        assert durability_type in DURABILITY_TYPES

        if not self.is_alive():
            # already dead, can't heal
            return

        if durability_type == "stun":
            self.db.stun = min(self.db.stun + damage, self.db.stun_max)
        elif durability_type == "wound":
            self.db.wound = min(self.db.wound + damage, self.db.wound_max)
            self.db.wound_post_first_aid = max(self.db.wound, self.db.wound_post_first_aid)
            self.db.wound_post_treatment = max(self.db.wound, self.db.wound_post_treatment)
        elif durability_type == "mortal":
            self.db.mortal = min(self.db.mortal + damage, self.db.mortal_max)
        elif durability_type == "fatigue":
            self.db.fatigue = min(self.db.fatigue + damage, self.db.fatigue_max)

    def take_damage(self, damage: int, durability_type: str, source: Optional[str] = None):
        """add the damage to our total, applying any damage overflows"""
        assert durability_type in DURABILITY_TYPES
        
        if not self.is_alive():
            # already dead, can't take more damage
            return
        
        # loop through the damages, adding any overflow back to the list
        pending_damage = [(damage, durability_type)]
        
        while len(pending_damage) > 0:
            damage, durability_type = pending_damage.pop()
            curr_health = self.attributes.get(durability_type, 0)
            curr_health -= damage
    
            overflow = 0
            if curr_health < 0:
                overflow = abs(curr_health) // 2
                curr_health = 0
    
            self.attributes.add(durability_type, curr_health)
    
            # apply damage overflow
            if overflow > 0:
                overflow_type = None
                if durability_type == "stun":
                    overflow_type = "wound"
                elif durability_type == "wound":
                    overflow_type = "mortal"
                elif durability_type == "mortal":
                    pass
                elif durability_type == "fatigue":
                    pass
                
                if overflow_type:
                    pending_damage.append((overflow, overflow_type))

    def check_durability(self):
        """Update durability-related statuses and such"""
        # check existing statuses
        check_unconscious_state = False
        if self.has_status("unconscious - stun"):
            if self.db.stun > 0:
                self.remove_status("unconscious - stun")
                self.remove_status("out cold")
                check_unconscious_state = True
        if self.has_status("unconscious - wound"):
            if self.db.wound > 0:
                self.remove_status("unconscious - wound")
                check_unconscious_state = True
        if self.has_status("unconscious - fatigue"):
            if self.db.fatigue > 0:
                self.remove_status("unconscious - fatigue")
                check_unconscious_state = True

        if check_unconscious_state:
            if not self.has_status_category("unconscious"):
                # time to wake up
                self.location.msg_contents("%s wakes up." % self)

        # check for new statuses
        if self.db.mortal <= 0:
            self.die()
            return

        if self.db.stun <= 0 and not self.has_status("unconscious - stun"):
            self.location.msg_contents("%s is |[xKnocked out|n💤" % self)
            self.add_status("unconscious - stun")
            self.add_status("out cold")

        if self.db.wound <= 0 and not self.has_status("unconscious - wound"):
            self.location.msg_contents("%s is |[xKnocked out|n💤💤" % self)
            self.add_status("unconscious - wound")

        if self.db.fatigue <= 0 and not self.has_status("unconscious - fatigue"):
            self.location.msg_contents("%s is |[xKnocked out|n💤💤💤" % self)
            self.add_status("unconscious - fatigue")

        if self.db.wound < self.db.wound_max \
                and not self.has_status("natural healing") \
                and not self.is_in_combat():
            self.add_status("natural healing")

    def die(self, source: str = None):
        """We have ceased to be"""
        if not source:
            source = "running out of health"

        self.location.msg_contents("%s is |X|[rDEAD|n💀" % self)
        log_info("%s has died." % self)
        self.db.combat_handler.remove_fighter(self)

        self.spawn_corpse()

        # obituary
        obit = \
"""
=======================================================================
You have died. You were killed by |530%s|n.
=======================================================================
""" % source

        self.msg(obit)
        self.respawn()

    def spawn_corpse(self, typeclass=None):
        """leave a corpse"""
        if not typeclass:
            typeclass = typeclasses.objects.Corpse

        corpse = typeclass.from_character(self)
        for item in self.contents:
            log_info("moving %s to corpse %s" % (item, corpse))
            item.move_to(corpse, quiet=True, emit_to_obj=self, use_destination=False)

    def respawn(self):
        # reset my stats
        self.db.respawn_count += 1

        self.db.stun = self.db.stun_max
        self.db.wound = self.db.wound_max
        self.db.mortal = self.db.mortal_max
        self.db.fatigue = self.db.fatigue_max

        self.db.wound_post_first_aid = self.db.wound_max
        self.db.wound_post_treatment = self.db.wound_max

        # send spawn message
        msg = """\
As your vision snaps out, you become aware of a thread that is attached to you. The thread is tied \
to a part of you that you don't often think about, but right now it's the only part of you that \
remains.

The thread quickly becomes taut. As it does, it starts to feel more like a cord than a thread. \
A few moments later it feels like it must be kind of cable to be able to pull so forcefully. \
Finally you realise it's actually a suit made of bone and flesh, and that you are standing naked in \
a |345cool blue|n light.

"""

        self.msg(msg)
        self.move_to(self.db.respawn_point)

    # equipment operations
    def get_equipment(self):
        """return a list of all our equipment"""
        return list(self.db.equipment)

    def get_equipped(self, equipment_type):
        """Return the item of the named type that is currently equipped

        :type equipment_type: "str"
        :rtype: typeclasses.objects.Equipment
        """
        for item in self.db.equipment:
            if item is None:
                log_info("BUG: I am equipped with something that has gone missing? %s" % self.db.equipment)
                self.db.equipment = [x for x in self.db.equipment if x]
                return None
            if item.equipment_type == equipment_type:
                return item
        return None
    
    def get_equipped_weapon(self):
        """:rtype: typeclasses.objects.Weapon"""
        return self.get_equipped("weapon")

    def get_equipped_armor(self):
        """:rtype: typeclasses.objects.Armor"""
        return self.get_equipped("armor")

    def is_equipped_with(self, item):
        """are we currently equipped with this item"""
        return (item in self.db.equipment)

    def equip(self, item):
        """start using a piece of equipment

        :type item: typeclasses.objects.Equipment
        """
        self.db.equipment.append(item)

    def unequip(self, item):
        """stop using a piece of equipment

        :type item: typeclasses.objects.Equipment
        """
        try:
            self.db.equipment.remove(item)
        except ValueError:
            log_info("%s tried to remove %s but it wasn't equipped" % (self, item))

    # combat
    def is_in_combat(self):
        return self.db.combat_handler is not None

    def set_combat_action(self, action_key: str, arg: Any, func, description: Optional[str] = None):
        """Queue up a combat action for our next phase"""
        if description is None:
            description = "%s %s" % (action_key, str(arg))
        self.db.combat_action = (action_key, func, arg)
        self.db.combat_action_description = description
    
    def get_combat_action(self) -> Optional[Tuple[str, Any]]:
        """Returns our queued combat action"""
        return self.db.combat_action

    def clear_combat_action(self):
        self.db.combat_action = None
        self.db.combat_action_description = ""

    def clean_combat_info(self):
        """Clean up all our internal combat info"""
        self.db.combat_handler = None
        self.db.combat_action = None
        self.db.combat_action_description = ""

    def do_prompt(self):
        """build and display an appropriate prompt line"""
        strings = []
        
        # display ammo
        weapon = self.get_equipped_weapon()
        if weapon:
            width = math.ceil(weapon.db.clip_size / 2)
            bullat, bullet = divmod(weapon.db.ammo, 2)
            strings.append("|[=f|w[" + "‖"*bullat + "||"*bullet + " "*(width-bullat-bullet) + "]|n")

        # combat action
        if self.db.combat_handler:
            description = self.db.combat_action_description
            if description:
                strings.append(" |530(%s)|n" % description)
            else:
                strings.append(" |530(no action)|n")

        # health
        if self.is_in_combat():
            strings.append(
                " " +
                rg_grad(self.db.stun / self.db.stun_max) + str(self.db.stun) + "s" +
                rg_grad(self.db.wound / self.db.wound_max) + str(self.db.wound) + "w" +
                rg_grad(self.db.mortal / self.db.mortal_max) + str(self.db.mortal) + "m" +
                "|n"
            )

        # send prompt
        strings.append(" > ")
        self.msg(prompt="".join(strings))

    # other evennia overrides
    def return_appearance(self, looker, **kwargs):
        # this is a clone of the base object look for now

        from evennia.utils import list_to_string
        from collections import defaultdict

        if not looker:
            return ""

        # get and identify all objects
        visible = (con for con in self.contents if con != looker and con.access(looker, "view"))
        things = defaultdict(list)
        for con in visible:
            key = con.get_display_name(looker)

            # things can be pluralized
            things[key].append(con)

        # get description, build string
        strings = []

        # name
        strings.append("|c%s|n" % self.get_display_name(looker))

        # description
        desc = self.db.desc
        if desc:
            strings.append(desc)

        # contents
        if things:
            # handle pluralization of things
            thing_strings = []
            for key, itemlist in sorted(things.items()):
                nitem = len(itemlist)
                if nitem == 1:
                    key, _ = itemlist[0].get_numbered_name(nitem, looker, key=key)
                else:
                    key = [item.get_numbered_name(nitem, looker, key=key)[1] for item in itemlist][0]
                thing_strings.append(key)

            strings.append("|wYou see:|n " + list_to_string(thing_strings))

        # equipped items
        equipment = [ x.get_display_name(looker) for x in self.get_equipment() ]
        if equipment:
            strings.append("%s is |wequipped with|n %s" % (
                self.get_display_name(looker), list_to_string(equipment),))

        return "\n".join(strings)

    def at_before_move(self, destination):
        """If this returns False-y, the move is cancelled"""
        super().at_before_move(destination)

        # TODO: can't easily flee combat?

        if self.is_in_combat():
            self.db.combat_handler.remove_fighter(self)
            self.msg("You have left the fracas!")

        return True

    def at_after_move(self, source_location):
        super().at_after_move(source_location)

        # join any combat in progress
        if self.location.db.combat_handler:
            self.location.db.combat_handler.add_fighter(self)
            self.msg("You have walked into a fight! Prepare for next round!")

        # Exits aren't children of our AltCommand class, so we have to manually show the prompt
        self.do_prompt()

    def at_post_puppet(self, **kwargs):
        super().at_post_puppet(**kwargs)

        # remind people about hints
        if not self.db.hints_enabled:
            self.msg("|530Hints:|n are currently turned off for this character. to turn hints on, type |chints|n.")

    def make_spawn_equipment(self):
        def spawn(p):
            p = p.copy()
            p["location"] = self
            item = evennia.prototypes.spawner.spawn(p)[0]
            log_info("character %s given %s" % (self, item))
            return item

        # equip my player
        for prototype in pregen_items[self.db.profession]:
            spawn(prototype)

    # status effects
    def get_all_status_effects(self):
        return self.db.status_effects.values()

    def has_status(self, status_type: str):
        assert status_type in all_status_effects
        return status_type in self.db.status_effects

    def has_status_category(self, status_category: str):
        """Do we have any of the status effects of a category"""
        assert status_category in all_status_categories
        return any((x in self.db.status_effects) for x in status_categories[status_category])

    def add_status(self, status_type: str, duration: typing.Optional[int] = None):
        """Set a status on this character. Duration is the number of ticks the status lasts.

        If duration is None, the effect will not expire by itself.
        """
        log_info("%s gains status %s%s" % (
            self, status_type, "" if duration is None else " %d ticks" % duration))
        self.db.status_effects[status_type] = StatusEffect(status_type, duration)

    def remove_status(self, status_type: str):
        """Remove a status effect if we have it"""
        assert status_type in all_status_effects
        if status_type in self.db.status_effects:
            log_info("%s loses status %s" % (self, status_type))
            del(self.db.status_effects[status_type])

    def tick_down_statuses(self):
        # advance the duration of all statuses by one
        for effect in tuple(self.db.status_effects.values()):
            if effect.duration is None:
                continue

            if effect.duration > 0:
                effect.duration -= 1
            else:
                self.remove_status(effect.status_type)









