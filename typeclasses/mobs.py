import random
from typing import Optional

from evennia import DefaultScript
from evennia import TICKER_HANDLER as tickerhandler
from evennia.utils import logger
import evennia.prototypes.spawner
from evennia.utils.logger import log_info

from typeclasses.characters import Character
import commands.combat
import typeclasses.objects
from typeclasses.scripts import CombatHandler

from world import prototypes
from world.dice import roll
from world.utils import chars_in_room, clamp

DAMAGE_IRE = 40
LOOK_IRE = 4
TICK_IRE = -3
MIN_IRE = 0
MAX_IRE = 100


class Mob(Character):
    """Evil Minion"""

    adjectives = ['industrial', 'mobile', 'autonomous', 'intelligent', 'humanoid', 'greasy', 'wet',
                  'real', 'controlled', 'sophisticated', 'simple', 'mechanical', 'legged', 'shiny',
                  'parallel', 'flexible', 'advanced', 'reconfigurable', 'modular', 'smart', 'keen',
                  'available', 'wheeled', 'surgical', 'mindless', 'programmed', 'giant', 'tiny',
                  'commercial', 'complex', 'domestic', 'future', 'redundant', 'rusty', 'holonomic',
                  'computerized', 'programmable', 'joint', 'generation', 'hydraulic', 'cafeteria',
                  'smaller', 'purpose', 'underwater', 'blue', 'rigid', 'welding', 'conventional',
                  'miniature', 'articulated', 'nonholonomic', 'cheerful', 'interactive', 'reactive',
                  'cartesian', 'driven', 'biomimetic', 'adaptive', 'electric', 'insecure', 'inept',
                  'heterogeneous', 'identical', 'biological', 'automated', 'electronic']

    def at_object_creation(self):
        super().at_object_creation()

        self.db.parent = None

        self.db.profession = "none"
        self.db.species = "robot"
        self.db.pacifist = False

        # robots are worse than people
        self.db.strength = roll(2, 5, +2)
        self.db.dexterity = roll(2, 5, +2)
        self.db.constitution = roll(2, 5, +2)
        self.db.intelligence = roll(2, 5, +2)
        self.db.will = roll(2, 5, +2)
        self.db.personality = roll(2, 5, +2)
        
        # "AI" helpers
        self.db.ire = 0
        self.db.ire_warn_threshold = 20
        self.db.ire_attack_threshold = 35
        
        # random things
        self.db.hints_enabled = False
        self.db.respawn_point = self.location
        
    # DEEP AI System
    def choose_target(self, preferred: Optional[Character] = None) -> Optional[Character]:
        """Chose a random target in our area, favouring the supplied target"""
        if preferred:
            return preferred

        targets = chars_in_room(self.location, exclude=self, species="human")
        if targets:
            return random.choice(targets)
        else:
            return None

    def modify_ire(self, mod: int, cause: Optional[Character] = None):
        """Add or remove ire. Can cause mob to attack."""
        if self.db.pacifist:
            return
        
        # do some type checking here as our callers may not have access to the Character class
        if cause is not None and not isinstance(cause, Character):
            cause = None

        # apply the ire
        old_ire = self.db.ire
        new_ire = clamp(MIN_IRE, old_ire + mod, MAX_IRE)
        if new_ire != old_ire:
            self.db.ire = new_ire
            log_info("%s ire is now %d" % (self, new_ire,))

        # take action based on increased ire
        if mod > 0 and self.db.ire >= self.db.ire_warn_threshold:
            target = self.choose_target(cause)

            if self.db.ire >= self.db.ire_attack_threshold and target:
                log_info("%s ire raised to attack threshold" % self)
                self.start_attack(target)
            else:
                log_info("%s ire raised to warn threshold" % self)
                if target:
                    self.location.msg_contents(
                        "%s takes a few steps towards %s, looking slightly annoyed." % (self, target), exclude=target)
                    target.msg(
                        "%st takes a few steps towards |Wyou|n, looking slightly annoyed." % (self,))
                else:
                    self.location.msg_contents(
                        "%s starts to move around in an agitated manner, as if slightly annoyed." % (self,))

    def start_attack(self, target: Character):
        """attacking a target """
        if not (self.is_alive() and self.get_equipped_weapon() and self.is_mobile()):
            # in no fit state to fight
            return

        if not self.is_in_combat():
            CombatHandler.start_combat(self.location)

        old_action = self.get_combat_action()
        if old_action is None or (old_action[0] != "attack" or old_action[2] != target):
            # change of target
            self.location.msg_contents(
                "%s squeals like an audio feedback loop and starts to attack %s" % (self, target),
                exclude=self)

        self.set_combat_action("attack", target, commands.combat.CmdAttack.combat_func)

    def at_desc(self, looker=None, **kwargs):
        """get angrier at those looking too long"""
        super().at_desc(looker=None, **kwargs)

        if looker:
            if isinstance(looker, Character):
                log_info("%s looking at bot %s" % (str(looker), self))
                self.modify_ire(LOOK_IRE)

                # also make others a bit angry
                for char in chars_in_room(self.location, exclude=self, species="robot"):
                    char.modify_ire(LOOK_IRE // 2, looker)

    # replace some player methods
    def die(self, source: str = None, quiet: bool = False):
        if not quiet:
            self.location.msg_contents("%s is |X|[rDESTROYED|n🤖️⚙️️" % self)
            log_info("%s has died." % self)

        if self.db.combat_handler:
            self.db.combat_handler.remove_fighter(self)

        if self.db.parent:
            self.db.parent.db.child = None
            self.db.parent = None
        
        if not quiet:
            self.spawn_corpse(typeclasses.objects.RobotCorpse)
        else:
            # get rid of my gear too
            for item in self.contents:
                log_info("quietly destroying %s from %s" % (item, self))
                item.delete()

        self.delete()

    def take_damage(self, damage: int, durability_type: str, source: Optional[str] = None):
        """Strap my DEEP AI to: me getting punched"""
        super().take_damage(damage, durability_type, source)

        target = self.choose_target()
        self.modify_ire(DAMAGE_IRE, target)
        for char in chars_in_room(self.location, exclude=self, species="robot"):
            char.modify_ire(DAMAGE_IRE // 2.5, target)

    def go_to_start_location(self):
        """my location is set by my spawner parent"""
        pass

    # obliterate some player methods
    def respawn(self):
        pass

    def do_prompt(self):
        pass

    def make_spawn_equipment(self):
        # TODO: Speciest
        pass
    
    
class CleanerMob(Mob):
    """Peaceful mob that recycles junk"""
    def at_object_creation(self):
        super().at_object_creation()
        
        self.db.pacifist = True

        self.db.floor_junk = None
        tickerhandler.add(150, self.at_tick)

    def at_tick(self):
        """fixate on any floor item we find, and if it's still here next tick, eat it"""
        if not self.is_mobile():
            return

        my_junk = self.db.floor_junk
        if my_junk and my_junk in self.location.contents:
            self.location.msg_contents("With a loud final crunch, %s finishes devouring %s " %
                              (self, my_junk))
            log_info("cleaner %s ate %s" % (self, self.db.floor_junk))

            # move the contents onto the floor
            for item in my_junk.contents:
                item.move_to(self.location, quiet=True)

            self.db.floor_junk.delete()
            self.db.floor_junk = None

        else:
            junk = [x for x in self.location.contents
                    if isinstance(x, typeclasses.objects.AltObject)
                    and x.access(self, "get")]

            if junk:
                chosen_junk = random.choice(junk)
                self.db.floor_junk = chosen_junk
                self.location.msg_contents(
                    "%s scoots over and hops onto %s! It begins to devour %s , "
                    "making a loud grinding noise." % (self, chosen_junk, chosen_junk)
                )
            else:
                # nothing to do
                self.location.msg_contents(
                    "%s rushes over to a previously unnoticed hatch in the floor. The hatch clanks "
                    "open and %s disappears." % (self, self)
                )
                log_info("cleaner %s is leaving" % self)
                self.die(quiet=True)


class MobSpawner(DefaultScript):
    """Makes Evil Minions"""

    def at_script_creation(self):
        """
        Called once, when the script is created.
        """
        self.key = "Mob Spawner"
        self.interval = 120  # seconds
        self.persistent = True

        self.db.child = None
        
    def spawn_bot(self, prototype: dict, decorate_key: bool = True):
        """create a new bot from a prototype

        If decorate_key is set, add a random adjective to the bot's name

        :rtype: typeclasses.mobs.Mob
        """
        prototype = prototype.copy()
        if decorate_key:
            prototype["key"] = " ".join([random.choice(Mob.adjectives), prototype["key"]])
        prototype["location"] = self.obj
        mob = evennia.prototypes.spawner.spawn(prototype)[0]

        spawn_msg = mob.db.spawn_msg
        if not spawn_msg:
            spawn_msg = "There is a quiet pop, and then a robot appears from nowhere!"

        log_info("Robot spawns: %s" % (mob,))
        self.obj.msg_contents(spawn_msg)

        # join a fight if in progress
        if self.obj.db.combat_hander:
            self.obj.db.combat_hander.add_fighter(mob)
        else:
            log_info("no combat to join in %s" % self.obj)
        
        return mob

    def at_repeat(self):
        try:
            mob = None

            # Spawn a new cleaner if we have orphaned items
            orphans = [x for x in self.obj.contents 
                       if isinstance(x, typeclasses.objects.AltObject)
                       and x.access(self, "get")]
            
            # check if there is already a cleaner here
            if orphans and not any(x for x in self.obj.contents if isinstance(x, CleanerMob)):
                log_info("Found junk for a bot to clean: %s" % str(orphans))
                mob = self.spawn_bot(prototypes.CLEANER_BOT, decorate_key=False)
            
            # look after our child
            if self.db.child:
                child = self.db.child
                if child.location == self.obj:
                    # all is right in the world
                    child.modify_ire(TICK_IRE)
                    return
                else:
                    # empty nest
                    log_info("%s's child (%s) has moved away. Disowning them." %
                             (self, child))
                    child.location.msg_contents(
                        "%s hops into a hidden security hatch in the floor and disappears." % child)
                    child.db.parent = None
                    self.db.child = None
                    child.die(quiet=True)
                    
            # Spawn a new security bot
            mob = self.spawn_bot(prototypes.SECURITY_BOT)
            self.db.child = mob
            mob.db.parent = self

            # equip my bot
            weapon_options = [
                prototypes.PISTOL_11MM_CHARGE,
                prototypes.LASER_PISTOL,
                prototypes.RENDER_RIFLE
            ]
            armor_options = [
                None,
                None,
                None,
                None,
                prototypes.BATTLE_VEST,
                prototypes.BATTLE_JACKET,
                prototypes.CARBON_FIBER_COAT,
                prototypes.GRAPHITE_FIBER_COAT
            ]

            for prototype in (random.choice(weapon_options), random.choice(armor_options)):
                if not prototype:
                    continue
                prototype = prototype.copy()
                prototype["location"] = mob
                item = evennia.prototypes.spawner.spawn(prototype)[0]
                log_info("mob %s equipped with %s" % (mob, item))
                mob.equip(item)

        except:
            logger.log_trace()


