from evennia.utils.logger import log_info

from evennia import CmdSet

from typeclasses.rooms import DefaultRoom
from commands.command import AltCommand


# Room for info on professions
class ProfessionInfoRoom(DefaultRoom):
    """Add commands to get info on professions"""
    def at_object_creation(self):
        "this is called only at first creation"
        self.cmdset.add(ProfessionInfoRoomCmdset, permanent=True)


class CmdProfessionInfo(AltCommand):
    """
    Get more info about an available profession by typing its name.

    Usage:
        combat spec
        free agent
        tech op

    """
    key = "profession info"
    aliases = ["combat spec", "tech op", "free agent", "combat", "tech", "free"]
    help_category = "character generation"

    def func(self):
        """Read off the description of the corresponding room object"""
        caller = self.caller

        # help a user out
        if self.cmdstring == self.key:
            caller.msg(type(self).__doc__)
            return

        try:
            profession = {
                "combat": "combat spec",
                "tech": "tech op",
                "free": "free agent",
            }[self.cmdstring]
        except KeyError:
            profession = self.cmdstring

        # fetch the description
        target = caller.search(profession, candidates=caller.location.contents, quiet=True, exact=True)
        try:
            # quiet=True means we get a list of results
            target = target[0]
        except IndexError:
            log_info("missing profession archetype %s" % profession)
        else:
            caller.msg(target.db.desc)


class ProfessionInfoRoomCmdset(CmdSet):
    key = "Profession Info Room"
    priority = 10

    def at_cmdset_creation(self):
        self.add(CmdProfessionInfo)


# Room for picking your profession
class PickProfessionRoom(DefaultRoom):
    """Add commands to pick your profession"""
    def at_object_creation(self):
        "this is called only at first creation"
        self.cmdset.add(PickProfessionRoomCmdset, permanent=True)


class CmdPickProfession(AltCommand):
    """
    Chose your profession by typing its name.

    Usage:
        combat spec
        free agent
        tech op

    """
    key = "pick profession"
    aliases = ["combat spec", "tech op", "free agent", "combat", "tech", "free"]
    help_category = "character generation"

    def func(self):
        caller = self.caller

        # help a user out
        if self.cmdstring == self.key:
            caller.msg(type(self).__doc__)
            return

        try:
            profession = {
                "combat": "combat spec",
                "tech": "tech op",
                "free": "free agent",
            }[self.cmdstring]
        except KeyError:
            profession = self.cmdstring

        # enact the choice
        log_info(f"{caller} has picked profession {profession}" )
        caller.pick_profession(profession)
        caller.msg("You are ready to start your adventure!")


class PickProfessionRoomCmdset(CmdSet):
    key = "Pick Profession Room"
    priority = 10

    def at_cmdset_creation(self):
        self.add(CmdPickProfession)
