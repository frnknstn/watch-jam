"""
Scripts

Scripts are powerful jacks-of-all-trades. They have no in-game
existence and can be used to represent persistent game systems in some
circumstances. Scripts can also have a time component that allows them
to "fire" regularly or a limited number of times.

There is generally no "tree" of Scripts inheriting from each other.
Rather, each script tends to inherit from the base Script class and
just overloads its hooks to have it perform its function.

"""

from evennia import DefaultScript
from evennia.utils import logger

import world.alternity
from evennia.utils.logger import log_info
from world.alternity import roll_combat_initiative
from world.utils import chars_in_room
import world.status #import do_char_combat_status_tick

class Script(DefaultScript):
    """
    A script type is customized by redefining some or all of its hook
    methods and variables.

    * available properties

     key (string) - name of object
     name (string)- same as key
     aliases (list of strings) - aliases to the object. Will be saved
              to database as AliasDB entries but returned as strings.
     dbref (int, read-only) - unique #id-number. Also "id" can be used.
     date_created (string) - time stamp of object creation
     permissions (list of strings) - list of permission strings

     desc (string)      - optional description of script, shown in listings
     obj (Object)       - optional object that this script is connected to
                          and acts on (set automatically by obj.scripts.add())
     interval (int)     - how often script should run, in seconds. <0 turns
                          off ticker
     start_delay (bool) - if the script should start repeating right away or
                          wait self.interval seconds
     repeats (int)      - how many times the script should repeat before
                          stopping. 0 means infinite repeats
     persistent (bool)  - if script should survive a server shutdown or not
     is_active (bool)   - if script is currently running

    * Handlers

     locks - lock-handler: use locks.add() to add new lock strings
     db - attribute-handler: store/retrieve database attributes on this
                        self.db.myattr=val, val=self.db.myattr
     ndb - non-persistent attribute handler: same as db but does not
                        create a database entry when storing data

    * Helper methods

     start() - start script (this usually happens automatically at creation
               and obj.script.add() etc)
     stop()  - stop script, and delete it
     pause() - put the script on hold, until unpause() is called. If script
               is persistent, the pause state will survive a shutdown.
     unpause() - restart a previously paused script. The script will continue
                 from the paused timer (but at_start() will be called).
     time_until_next_repeat() - if a timed script (interval>0), returns time
                 until next tick

    * Hook methods (should also include self as the first argument):

     at_script_creation() - called only once, when an object of this
                            class is first created.
     is_valid() - is called to check if the script is valid to be running
                  at the current time. If is_valid() returns False, the running
                  script is stopped and removed from the game. You can use this
                  to check state changes (i.e. an script tracking some combat
                  stats at regular intervals is only valid to run while there is
                  actual combat going on).
      at_start() - Called every time the script is started, which for persistent
                  scripts is at least once every server start. Note that this is
                  unaffected by self.delay_start, which only delays the first
                  call to at_repeat().
      at_repeat() - Called every self.interval seconds. It will be called
                  immediately upon launch unless self.delay_start is True, which
                  will delay the first call of this method by self.interval
                  seconds. If self.interval==0, this method will never
                  be called.
      at_stop() - Called as the script object is stopped and is about to be
                  removed from the game, e.g. because is_valid() returned False.
      at_server_reload() - Called when server reloads. Can be used to
                  save temporary variables you want should survive a reload.
      at_server_shutdown() - called at a full server shutdown.

    """

    pass


class CombatHandler(Script):
    """
    Handles the fight in the room
    """

    def at_script_creation(self):
        """
        Called once, when the script is created.
        """
        self.key = "Combat Handler"
        self.interval = 7  # seconds
        self.persistent = True
        self.start_delay = True

        self.db.turn = 1
        self.db.phase = 0   # planning phase, only at start of combat

        # track which fighters have ever been in the fight
        self.db.fighter_stats = {}
        self.db.initiative_table = None

        # Add a reference to this script to the room
        self.obj.db.combat_handler = self

        # Pull all characters into this fight
        for character in chars_in_room(self.obj):
            self.add_fighter(character)

        # start the first turn in a planning phase
        self.start_turn()
        self.obj.msg_contents("|x== |WGet Ready! Phase start in %d seconds|x ==|n" % self.interval)

    def at_stop(self):
        """Clean up"""
        self.obj.msg_contents("|[C==! |WCombat Over !==|n")
        self.obj.msg_contents(("|[CFight lasted %d turns|n" % self.db.turn,  {'type': 'initiative'}))
        self.obj.db.combat_handler = None

        for character in chars_in_room(self.obj):
            self.remove_fighter(character)

            # wake up?
            if character.is_alive() \
               and character.has_status("unconscious - stun") \
               and not character.has_status("out cold"):
                character.db.stun = 1
                character.check_durability()

    def at_repeat(self):
        """Advance to the next phase or turn"""
        # go to the next phase or turn
        try:
            # check if combat is over
            if not any(x.get_combat_action() for x in chars_in_room(self.obj)):
                # nobody has anything to do
                log_info("Combat over due to no actions")
                self.stop()
                return
            
            # do the phases
            self.db.phase += 1
            if self.db.phase > 4:
                self.db.turn += 1
                self.db.phase = 1
                self.start_turn()
            self.start_phase()
        except:
            logger.log_trace()

    def start_turn(self):
        """Run at the start of a new turn"""
        self.obj.msg_contents("|[C|x=== |WCombat: Turn %d|x ===|n" % self.db.turn)
        roll_combat_initiative(self.obj, self)

    def start_phase(self):
        """Run at the start of a new turn"""

        def phase_helper(character):
            """returns a colored string showing the current phase and when a char can act"""
            strings = []
            for phase in (1, 2, 3, 4):
                if character not in self.db.initiative_table[phase]:
                    color = "|C"
                else:
                    color = "|c"

                if phase != self.db.phase:
                    strings.append(" %s%d " % (color, phase))
                else:
                    strings.append("|W[%s%d|W]" % (color, phase))
            strings.append("|n")

            return "".join(strings)

        # display customised phase messages to players
        for character in (x for x in chars_in_room(self.obj) if x.db.species == "human"):
            # TODO: Speciest
            character.msg("|x== |WCombat Phase %s|x ==|n" % (phase_helper(character)))

        # do actions
        initiative = self.db.initiative_table[self.db.phase]
        for character in initiative:
            if not character:
                log_info("maybe this char left combat dead?")
                continue

            if character.db.combat_handler != self:
                # character is no longer in this combat
                continue

            if character.has_status_category("unconscious"):
                # can't do anything while knocked out
                continue

            if not character.get_combat_action():
                character.msg("You don't do anything.")
                continue

            # do the queued action
            action_key, func, arg = character.get_combat_action()
            logger.log_info("%s - %s combat action: %s %s" % (self, character, action_key, str(arg)))

            if action_key == "attack":
                func(character, arg)

            else:
                raise Exception("Unknown combat action '%s' (%s) " % (action_key, str(arg)))

        # apply the new status effects, tick status effects, or just die
        for character in chars_in_room(self.obj):
            world.status.do_char_combat_status_tick(character)
            
    def add_fighter(self, character):
        """Add a character to this fight"""
        log_info("adding character %s to combat %s" % (character, self.dbref))
        character.db.combat_handler = self
        self.obj.msg_contents("|c%s|n joins the fray" % character)

    def remove_fighter(self, character):
        """Remove a char from this fight"""
        log_info("Calling remove_fighter for %s in fight %s" % (character, self.dbref))
        character.clean_combat_info()
        
        if character.is_alive():
            character.check_durability()

        if not self.obj.db.combat_handler:
            # fight is ending
            return

        # does the fight carry on without them?
        character.msg(("|[CYou have left the fight|n", {'type': 'initiative'}))

        left = [x for x in chars_in_room(self.obj) if x != character]

        self.obj.msg_contents("%s leaves the fight (%d left)" % (character, len(left)))

        # humans vs robits
        # todo: speciest
        humans = 0
        robots = 0
        for char in left:
            if char.db.species == "robot" and not char.db.pacifist:
                robots += 1
            else:
                humans += 1

        # is nobody left to fight?
        log_info("there are %d humans and %d robots left in the fight" % (humans, robots))
        if robots == 0 or humans == 0:
            log_info("Ending combat due to lack of participants")
            self.obj.msg_contents("There is nobody left to fight, the combat is over.")
            self.stop()

    @classmethod
    def start_combat(cls, location):
        """It goes down now!"""
        if location.db.combat_handler:
            raise Exception("There is already a combat handler here")

        location.scripts.add(CombatHandler)




