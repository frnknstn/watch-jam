"""
Room

Rooms are simple containers that has no location of their own.

"""

from evennia.utils.logger import log_info
from evennia import DefaultRoom


class Room(DefaultRoom):
    """
    Rooms are like any Object, except their location is None
    (which is default). They also use basetype_setup() to
    add locks so they cannot be puppeted or picked up.
    (to change that, use at_object_creation instead)

    See examples/object.py for a list of
    properties and methods available on all Objects.
    """

    pass


class SpawnRoom(DefaultRoom):
    """Entering this room sets it as a character's home location"""
    def at_object_receive(self, moved_obj, source_location, **kwargs):
        super().at_object_receive(moved_obj, source_location, **kwargs)

        # set our new spawn point
        new_spawn = self.db.new_spawn or self
        if moved_obj.has_account and not moved_obj.is_superuser:
            moved_obj.db.respawn_point = new_spawn
            log_info("Set '%s' spawn point to '%s'" % (moved_obj, moved_obj.db.respawn_point))

            moved_obj.msg('''\
As soon as your foot touches the floor, a reedy, electronic voice says: 
  "Welcome back, |C%s|n. Your Redbury Clone account is still in arrears. Make a payment as soon as \
possible or risk losing your coverage."''' % moved_obj
            )

