"""
Object

The Object is the "naked" base class for things in the game world.

Note that the default Character, Room and Exit does not inherit from
this Object, but from their respective default implementations in the
evennia library. If you want to use this class as a parent to change
the other types, you can do so by adding this as a multiple
inheritance.

"""
from evennia import DefaultObject
from evennia import CmdSet
from evennia.utils.logger import log_info
from evennia.utils.search import search_object

import evennia.objects.objects

from world.alternity import DamageDice, IMPACT_TYPES
from world.dice import DiceSet, AlternityCheck

import commands.command

# check that our object inheritance matches the built-in?? Or something? We don't want to lose Pycharm hints.
from world.utils import chars_in_room

assert (evennia.objects.objects.DefaultObject == DefaultObject)


class Object(evennia.objects.objects.DefaultObject):
    """
    This is the root typeclass object, implementing an in-game Evennia
    game object, such as having a location, being able to be
    manipulated or looked at, etc. If you create a new typeclass, it
    must always inherit from this object (or any of the other objects
    in this file, since they all actually inherit from BaseObject, as
    seen in src.object.objects).

    The BaseObject class implements several hooks tying into the game
    engine. By re-implementing these hooks you can control the
    system. You should never need to re-implement special Python
    methods, such as __init__ and especially never __getattribute__ and
    __setattr__ since these are used heavily by the typeclass system
    of Evennia and messing with them might well break things for you.


    * Base properties defined/available on all Objects

     key (string) - name of object
     name (string)- same as key
     dbref (int, read-only) - unique #id-number. Also "id" can be used.
     date_created (string) - time stamp of object creation

     account (Account) - controlling account (if any, only set together with
                       sessid below)
     sessid (int, read-only) - session id (if any, only set together with
                       account above). Use `sessions` handler to get the
                       Sessions directly.
     location (Object) - current location. Is None if this is a room
     home (Object) - safety start-location
     has_account (bool, read-only)- will only return *connected* accounts
     contents (list of Objects, read-only) - returns all objects inside this
                       object (including exits)
     exits (list of Objects, read-only) - returns all exits from this
                       object, if any
     destination (Object) - only set if this object is an exit.
     is_superuser (bool, read-only) - True/False if this user is a superuser

    * Handlers available

     aliases - alias-handler: use aliases.add/remove/get() to use.
     permissions - permission-handler: use permissions.add/remove() to
                   add/remove new perms.
     locks - lock-handler: use locks.add() to add new lock strings
     scripts - script-handler. Add new scripts to object with scripts.add()
     cmdset - cmdset-handler. Use cmdset.add() to add new cmdsets to object
     nicks - nick-handler. New nicks with nicks.add().
     sessions - sessions-handler. Get Sessions connected to this
                object with sessions.get()
     attributes - attribute-handler. Use attributes.add/remove/get.
     db - attribute-handler: Shortcut for attribute-handler. Store/retrieve
            database attributes using self.db.myattr=val, val=self.db.myattr
     ndb - non-persistent attribute handler: same as db but does not create
            a database entry when storing data

    * Helper methods (see src.objects.objects.py for full headers)

     search(ostring, global_search=False, attribute_name=None,
             use_nicks=False, location=None, ignore_errors=False, account=False)
     execute_cmd(raw_string)
     msg(text=None, **kwargs)
     msg_contents(message, exclude=None, from_obj=None, **kwargs)
     move_to(destination, quiet=False, emit_to_obj=None, use_destination=True)
     copy(new_key=None)
     delete()
     is_typeclass(typeclass, exact=False)
     swap_typeclass(new_typeclass, clean_attributes=False, no_default=True)
     access(accessing_obj, access_type='read', default=False)
     check_permstring(permstring)

    * Hooks (these are class methods, so args should start with self):

     basetype_setup()     - only called once, used for behind-the-scenes
                            setup. Normally not modified.
     basetype_posthook_setup() - customization in basetype, after the object
                            has been created; Normally not modified.

     at_object_creation() - only called once, when object is first created.
                            Object customizations go here.
     at_object_delete() - called just before deleting an object. If returning
                            False, deletion is aborted. Note that all objects
                            inside a deleted object are automatically moved
                            to their <home>, they don't need to be removed here.

     at_init()            - called whenever typeclass is cached from memory,
                            at least once every server restart/reload
     at_cmdset_get(**kwargs) - this is called just before the command handler
                            requests a cmdset from this object. The kwargs are
                            not normally used unless the cmdset is created
                            dynamically (see e.g. Exits).
     at_pre_puppet(account)- (account-controlled objects only) called just
                            before puppeting
     at_post_puppet()     - (account-controlled objects only) called just
                            after completing connection account<->object
     at_pre_unpuppet()    - (account-controlled objects only) called just
                            before un-puppeting
     at_post_unpuppet(account) - (account-controlled objects only) called just
                            after disconnecting account<->object link
     at_server_reload()   - called before server is reloaded
     at_server_shutdown() - called just before server is fully shut down

     at_access(result, accessing_obj, access_type) - called with the result
                            of a lock access check on this object. Return value
                            does not affect check result.

     at_before_move(destination)             - called just before moving object
                        to the destination. If returns False, move is cancelled.
     announce_move_from(destination)         - called in old location, just
                        before move, if obj.move_to() has quiet=False
     announce_move_to(source_location)       - called in new location, just
                        after move, if obj.move_to() has quiet=False
     at_after_move(source_location)          - always called after a move has
                        been successfully performed.
     at_object_leave(obj, target_location)   - called when an object leaves
                        this object in any fashion
     at_object_receive(obj, source_location) - called when this object receives
                        another object

     at_traverse(traversing_object, source_loc) - (exit-objects only)
                              handles all moving across the exit, including
                              calling the other exit hooks. Use super() to retain
                              the default functionality.
     at_after_traverse(traversing_object, source_location) - (exit-objects only)
                              called just after a traversal has happened.
     at_failed_traverse(traversing_object)      - (exit-objects only) called if
                       traversal fails and property err_traverse is not defined.

     at_msg_receive(self, msg, from_obj=None, **kwargs) - called when a message
                             (via self.msg()) is sent to this obj.
                             If returns false, aborts send.
     at_msg_send(self, msg, to_obj=None, **kwargs) - called when this objects
                             sends a message to someone via self.msg().

     return_appearance(looker) - describes this object. Used by "look"
                                 command by default
     at_desc(looker=None)      - called by 'look' whenever the
                                 appearance is requested.
     at_get(getter)            - called after object has been picked up.
                                 Does not stop pickup.
     at_drop(dropper)          - called when this object has been dropped.
     at_say(speaker, message)  - by default, called if an object inside this
                                 object speaks

    """
    pass


class AltObject(Object):
    """Base class for all actual /items/ in the game"""
    pass


# things that make bots angry if you mess with them
class SacredObject(Object):
    """Mixin for things that make bots angry if you mess with them.

    Just set self.db.sacred_ire_mod to a number if you want, and call
    self.modify_local_ire() whenever someone does something angry-making.
    """
    DEFAULT_IRE_MOD = 6

    def modify_local_ire(self, mod: int = None, cause=None):
        """raise the ire of all local irritable things

        :type mod: int
        :type cause: typeclasses.characters.Character or None
        """
        if mod is None:
            mod = self.attributes.get("sacred_ire_mod", self.DEFAULT_IRE_MOD)

        log_info("%s raising ire of room by %d" % (self, mod))

        for char in chars_in_room(self.location, species="robot"):
            char.modify_ire(mod, cause)

    def at_desc(self, looker=None, **kwargs):
        super().at_desc(looker=None, **kwargs)

        log_info("%s looking at sacred object %s" % (str(looker), self))
        self.modify_local_ire(cause=looker)


# skill-specific objects
class BrokenObject(AltObject, SacredObject):
    """Object that teleports a character to a new location when it is "repaired"

    Important attributes:
      required_repairs - number of repairs needed
      repairs - number of repair actions so far
      success_teleport_to - where to teleport in case of success
      success_teleport_msg - message to echo while teleporting to success
      failure_teleport_to - where to teleport to in case of failure
      failure_teleport_msg - message to echo while teleporting to failure
    """

    def at_object_creation(self):
        super(BrokenObject, self).at_object_creation()

        self.db.reset_repairs = 0
        self.db.required_repairs = 6
        self.db.success_teleport_to = None
        self.db.success_teleport_msg = ""
        self.db.failure_teleport_to = ""
        self.db.failure_teleport_msg = ""

    def return_appearance(self, looker, **kwargs):
        """Show how damaged the object is"""
        retval = super(BrokenObject, self).return_appearance(looker)
        desc = self.repair_status_appearance(looker, **kwargs)
        return retval + "\n\n" + desc

    def repair_status_appearance(self, looker, **kwargs):
        if self.db.required_repairs >= 9:
            desc = "It is so badly damaged as to be barely recognisable. It will requires a major overhaul."
        elif self.db.required_repairs >= 7:
            desc = "It is very badly damaged. It requires extensive repairs."
        elif self.db.required_repairs >= 5:
            desc = "It is badly damaged. It requires extensive repairs."
        elif self.db.required_repairs >= 3:
            desc = "It is damaged, and needs some repairs before it will operate."
        elif self.db.required_repairs > 0:
            desc = "It is slight damaged, but some minor repairs will return it to working order."
        elif self.db.required_repairs == 0:
            desc = "It has been repaired and is now in working order."
        else:
            desc = "It's hard to tell how damaged it is."

        return desc

    def attempt_repair(self, caller):
        if self.db.required_repairs <= 0:
            return

        target_value, step = caller.get_skill_score("Repair")

        check = AlternityCheck(target_value, step)
        caller.location.msg_contents("|c%s|n attempts some repairs on |c%s|n: %s" % (
            caller,
            self,
            check.result_description()
        ))

        if check.is_ordinary:
            progress = 1
        elif check.is_good:
            progress = 2
        elif check.is_amazing:
            progress = 3
        else:
            progress = 0

        self.db.required_repairs = max(self.db.required_repairs - progress, 0)

        if self.db.required_repairs > 0:
            self.modify_local_ire(cause=caller)
            if progress > 0:
                caller.msg("You have made some progress but still need to do more. You pause to examine the %s. %s" %(
                    self, self.repair_status_appearance(caller)
                ))
        else:
            caller.msg("Repairs are now complete.")
            if self.db.success_teleport_to != "":
                # find the teleport target
                teleport_targets = search_object(self.db.success_teleport_to)
                if not teleport_targets:
                    print("no valid teleport target for %s was found." % self.db.success_teleport_to)
                    return
                elif len(teleport_targets) > 1:
                    print("found more than one teleport target, aborting.")
                    return
                teleport_target = teleport_targets[0]

                # do the teleport of all contents
                for con in (x for x in self.location.contents if not x.destination and x != self):
                    con.msg(self.db.success_teleport_msg)
                    con.move_to(teleport_target, quiet=True)

                # reset the puzzle
                self.db.required_repairs = self.db.reset_repairs


class Equipment(AltObject):
    """An object that can be equipped to a character"""
    equipment_types = ("item", "weapon", "armor")
    equipment_type = "item"

    def at_object_creation(self):
        self.db.mass = 0
        self.db.cost = 0

        # make sure our child classes are of valid types
        assert self.equipment_type in self.equipment_types

    # def at_before_give(self, giver, getter, **kwargs):
    #     # TODO: prevent equipped item from being dropped in combat maybe?
    #     return True
    #
    # def at_before_drop(self, dropper, **kwargs):
    #     # TODO: prevent equipped item from being dropped in combat maybe?
    #     return True

    def at_drop(self, dropper, **kwargs):
        # unlink us from our holder
        if dropper.is_equipped_with(self):
            dropper.unequip(self)

    def at_give(self, giver, getter, **kwargs):
        # unlink us from our holder
        if giver.is_equipped_with(self):
            giver.unequip(self)

    def at_after_move(self, source_location, **kwargs):
        # unlink us from our holder
        try:
            if source_location.is_equipped_with(self):
                source_location.unequip(self)
        except AttributeError:
            # we didn't come from a character, no need to unequip
            pass

        super().at_after_move(source_location, **kwargs)


class Weapon(Equipment):
    """A ranged weapon"""
    equipment_type = "weapon"

    def at_object_creation(self):
        super().at_object_creation()

        self.db.weapon_class = "rifle"
        self.db.damages = (DamageDice(1, 4, 0, "stun"),
                           DamageDice(1, 6, 0, "wound"),
                           DamageDice(1, 8, 0, "mortal"))
        self.db.impact = "HI"
        self.db.accuracy_penalty = 0
        self.db.ranges = (25, 50, 75)
        self.db.clip_size = 15
        self.db.ammo = self.db.clip_size
        self.db.ammo_type = "11mm"
        self.db.burst_shot_count = 3
        self.db.fire_modes = "FBA"

    def reload(self):
        self.db.ammo = self.db.clip_size


class Armor(Equipment):
    """A set of armor"""
    equipment_type = "armor"

    def at_object_creation(self):
        super().at_object_creation()

        self.db.damages = (DiceSet(1, 6, -3),
                           DiceSet(1, 6, -2),
                           DiceSet(1, 4, -2))
        self.db.hide = +2
        self.db.skill = None
        self.db.action_penalty = 0

    def impact_dice(self, impact):
        """Get the corresponding dice set that matches a supplied impact type

        :type impact: str
        :rtype: DiceSet
        """
        # sorted by most common case
        if impact == "HI":
            return self.db.damages[1]
        elif impact == "LI":
            return self.db.damages[0]
        elif impact == "En":
            return self.db.damages[2]
        else:
            assert impact in IMPACT_TYPES


# containers
class Corpse(AltObject):
    """A dead body"""
    def at_object_creation(self):
        super().at_object_creation()
        self.cmdset.add_default(LootableCmdSet, permanent=True)

    @classmethod
    def from_character(cls, character):
        """Drop a corpse for this character"""
        name = "corpse of %s" % character.key
        aliases = ["%s's corpse" % character.key, "corpse"]
        description = "Ew.\n|x(if there is stuff on this corpse, you can |wloot|x it)|n"
        corpse, _ = cls.create(
            name, aliases=aliases, location=character.location, description=description)
        return corpse


class RobotCorpse(Corpse, SacredObject):
    DEFAULT_IRE_MOD = 3


class LootableCmdSet(CmdSet):
    """
    Item that can be looted
    """
    key = "Lootable"
    duplicates = False

    def at_cmdset_creation(self):
        self.add(commands.command.CmdLoot())
