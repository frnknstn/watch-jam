from evennia import DefaultCharacter

from commands.command import AltCommand
from typeclasses.objects import Equipment, Weapon
from typeclasses.scripts import CombatHandler

import world.alternity

class CombatCommand(AltCommand):
    """Commands that happen at combat action intervals

    There are a ton of cool things I can do to remove boilerplate etc, but this is a jam.
        Just git 'er done.
    """

    # can we do this command only when in combat?
    combat_only = False

    def at_pre_cmd(self):
        """If this returns anything truthy, execution is aborted."""
        if self.combat_only and not self.caller.is_in_combat():
            self.caller.msg("you can only do use this command while in combat")
            return True

        return super().at_pre_cmd()

    def queue_action(self, arg, description):
        if arg is None and description is None:
           self.caller.clear_combat_action()
           self.caller.msg("You prepare to |530do nothing|n")
        else:
            self.caller.set_combat_action(self.key, arg, self.combat_func, description)
            self.msg("You prepare to |530%s|n" % description)


class CmdEquip(CombatCommand):
    """
    Start using a piece of combat equipment

    Usage:
      equip <weapon or armor>

    Select a weapon or armor to use. If you are already equipped with an
    item of the same type, you will change to the one you specified
    instead. You can stop using equipment with the "unequip" command.
    """

    key = "equip"
    aliases = ["wear", "don", "wield", "hold"]
    help_category = "combat"
    lock = "cmd:all()"

    def func(self):
        # validate our args
        if not self.args:
            self.caller.msg("Usage: equip <obj>")
            return

        item = self.caller.search(self.args, candidates=self.caller.contents)
        if not item:
            # search will alert the user why it couldn't find an item
            return

        if not isinstance(item, Equipment):
            self.caller.msg("That isn't equipment!")
            return

        # Check if we already using equipment of this type
        curr_item = self.caller.get_equipped(item.equipment_type)

        if not curr_item:
            self.caller.equip(item)
            self.caller.location.msg_contents("%s equips %s" % (self.caller, item))
        elif item == curr_item:
            self.caller.msg("You are already using %s" % (item,))
        else:
            self.caller.unequip(curr_item)
            self.caller.equip(item)
            self.caller.location.msg_contents("%s changes %s from %s to %s" % (
                self.caller, item.equipment_type, curr_item, item))


class CmdUnequip(CombatCommand):
    """
    Start using a piece of combat equipment

    Usage:
      equip <weapon or armor>

    Select a weapon or armor to use. If you are already equipped with an
    item of the same type, you will change to the one you specified
    instead. You can stop using equipment with the "unequip" command.
    """

    key = "unequip"
    aliases = ["doff", "unwield"]
    help_category = "combat"
    lock = "cmd:all()"

    def func(self):
        # validate our args
        if not self.args:
            self.caller.msg("Usage: unequip <obj>")
            return

        item = self.caller.search(self.args, candidates=self.caller.get_equipment())
        if not item:
            # search will alert the user why it couldn't find an item
            return

        self.caller.unequip(item)
        self.caller.location.msg_contents("%s unequips %s" % (self.caller, item))


class CmdAttack(CombatCommand):
    """
    Attack a character with your equipped weapon

    Usage:
      attack <character>

    TODO: This description
    """

    key = "attack"
    aliases = ["shoot", "hit"]
    help_category = "combat"
    lock = "cmd:all()"

    def func(self):
        caller = self.caller

        # validate our args
        if not self.args:
            caller.msg("Usage: %s <character>" % self.cmdstring)
            return

        target = caller.search(self.args, candidates=caller.location.contents)
        if not target:
            # search will alert the user why it couldn't find an item
            return

        # can't hit self
        if target == caller:
            caller.msg("Usage: %s <character>" % self.cmdstring)
            return

        if not isinstance(target, DefaultCharacter) or target.db.species != "robot":
            # TODO: speciest
            caller.msg("You can only %s mobs"  % self.cmdstring)
            return

        if not caller.get_equipped_weapon():
            caller.msg("You can't %s, you don't have an equipped weapon" % self.cmdstring)
            return

        # does it ~go down~ now
        if not caller.is_in_combat():
            CombatHandler.start_combat(caller.location)

        # queue the attack
        arg = target
        description = "%s %s" % (self.key, str(arg))
        self.queue_action(arg, description)

    @staticmethod
    def combat_func(character, arg):
        # check the target is still valid
        if not arg or arg.location != character.location:
            character.msg("Your target is gone.")
            character.clear_combat_action()
            return

        # do attack
        world.alternity.do_attack(character, arg)


class CmdReload(CombatCommand):
    """
    Reload your weapon

    Usage:
      equip <weapon>

    Reload the named weapon, or your equipped weapon if none is
    specified.
    """

    key = "reload"
    aliases = []
    help_category = "combat"
    lock = "cmd:all()"

    def func(self):
        # validate our args
        if not self.args:
            # default to our equipped weapon
            weapon = self.caller.get_equipped_weapon()
            if not weapon:
                self.caller.msg("Which weapon do you want to reload?")
                return
        else:
            weapon = self.caller.search(self.args,
                candidates = [x for x in self.caller.get_equipment() if isinstance(x, Weapon)])
            if not weapon:
                # search will alert the user why it couldn't find an item
                return

        weapon.reload()
        self.caller.location.msg_contents("%s reloads %s" % (self.caller, weapon))


class CmdWait(CombatCommand):
    """
    Do nothing on your combat action

    Usage:
      wait

    Clears your queued combat action.
    """
    key = "wait"
    aliases = []
    help_category = "combat"

    combat_only = True

    def func(self):
        self.queue_action(None, None)


class CmdCombat(AltCommand):
    """
    Debug command for combat

    Usage:
      combat [actions]

    Available actions:
      combat start
      combat stop
    """
    key = "combat"
    help_category = "combat"

    def func(self):
        caller = self.caller
        if not self.args:
            # need an sub-action
            caller.msg("You need to provide a sub-action for the 'combat' debug command.")
            caller.msg(self.__doc__)
            return
        
        location = caller.location
        
        # get our sub-actions
        if self.args == "start":
            if location.db.combat_handler:
                caller.msg("There is already a combat handler here")
                return
            caller.msg("starting combat")
            location.scripts.add(CombatHandler)

        elif self.args in ("end", "stop"):
            if not location.db.combat_handler:
                caller.msg("There is no combat here")
                return
            caller.msg("ending combat")
            location.db.combat_handler.stop()

        elif self.args.startswith("kill "):
            lhs, rhs = self.args.split(" ", 1)

            target = caller.search(rhs, candidates=caller.location.contents)
            if not target:
                # search will alert the user why it couldn't find an item
                return

            caller.location.msg_contents("Killing %s" % target)
            target.die("combat test command")

        elif self.args == "test":
            import world.alternity
            world.alternity.roll_combat_initiative(self.caller.location, self.caller.location.db.combat_handler)

        else:
            self.caller.msg("unknown combat command '%s'" % self.args)

# self.caller.msg(
# """
# ╒══════════════════════════════════════╕
# │ Initiative                           │
# ├──────────────────────────────────────┤
# │ Phase 1:                             │
# ├──────────────────────────────────────┤
# │ Phase 2:                             │
# ├──────────────────────────────────────┤
# │ Phase 3:                             │
# ├──────────────────────────────────────┤
# │ Phase 4:                             │
# ├──────────────────────────────────────┤
# │                                      │
# └──────────────────────────────────────┘
# """)
#
