"""
Commands

Commands describe the input the account can do to the game.

"""

from evennia import Command as BaseCommand
from evennia.commands.default.muxcommand import MuxCommand
from evennia import DefaultCharacter
from evennia.utils import list_to_string

from world.dice import AlternityCheck


class Command(BaseCommand):
    """
    Inherit from this if you want to create your own command styles
    from scratch.  Note that Evennia's default commands inherits from
    MuxCommand instead.

    Note that the class's `__doc__` string (this text) is
    used by Evennia to create the automatic help entry for
    the command, so make sure to document consistently here.

    Each Command implements the following methods, called
    in this order (only func() is actually required):
        - at_pre_cmd(): If this returns anything truthy, execution is aborted.
        - parse(): Should perform any extra parsing needed on self.args
            and store the result on self.
        - func(): Performs the actual work.
        - at_post_cmd(): Extra actions, often things done after
            every command, like prompts.

    """

    pass


# -------------------------------------------------------------
#
# The default commands inherit from
#
#   evennia.commands.default.muxcommand.MuxCommand.
#
# If you want to make sweeping changes to default commands you can
# uncomment this copy of the MuxCommand parent and add
#
#   COMMAND_DEFAULT_CLASS = "commands.command.MuxCommand"
#
# to your settings file. Be warned that the default commands expect
# the functionality implemented in the parse() method, so be
# careful with what you change.
#
# -------------------------------------------------------------


# base command class for all commands
class AltCommand(MuxCommand):
    def at_post_cmd(self):
        if isinstance(self.caller, DefaultCharacter):
            try:
                self.caller.do_prompt()
            except NameError:
                pass


class CmdAltRoll(AltCommand):
    """Perform an Alternity ability check

    Usage:
        altroll <target> <challenge level> [trivial?]

    Roll a check, trying to hit a particular target number with a challenge
    level.

    If the check is trivial enough that failure is impossible, specify
    "trivial" and any failure will instead be a marginal success.
    """
    key = "alternity roll"
    aliases = ["altroll"]
    help_category = "Alternity"

    def usage(self):
        self.caller.msg("Usage:\n  altroll <target value> <challenge level -5 to 10> [trivial]")

    def func(self):
        args = self.args.strip()
        caller = self.caller

        # parse the args
        if args == "":
            self.usage()
            return

        tokens = args.split()
        if len(tokens) < 1 or len(tokens) > 3:
            self.usage()
            return

        try:
            target_value = int(tokens[0])
        except ValueError:
            self.usage()
            return

        situation_step = 0
        trivial = False

        if len(tokens) >= 2:
            try:
                situation_step = int(tokens[1])
            except ValueError:
                self.usage()
        if len(tokens) == 3 and tokens[2].lower().startswith("t"):
            trivial = True

        # do the roll and check the results
        result = AlternityCheck(target_value, situation_step, trivial)

        message = "%s rolls a %s check of %d at %s: %s (%s)" % (
            caller.name,
            "trivial" if trivial else "skill",
            target_value,
            result.situation_description(),
            result.result_description(),
            result.roll_description()
        )

        # publish the results
        caller.location.msg_contents(message)


class CmdLoot(AltCommand):
    """
    loot an item, putting the contents onto the floor

    Usage:
      loot <item>

    Some objects like containers or dead bodies can be hastily turned-out,
    resulting in their contents being spilled onto the floor.
    """
    key = "loot"
    help_category = "General"

    def func(self):
        caller = self.caller
        if not self.args:
            caller.msg("What do you want to loot?")
            return

        obj = caller.search(self.args, candidates=caller.location.contents)
        if not obj:
            # search will alert the user why it couldn't find an item
            return

        if not isinstance(obj, type(self.obj)):
            caller.msg("You cannot loot that.")
            return

        if not obj.contents:
            caller.msg("You riffle around in %s but don't find anything useful." % obj)
            return

        # do the loot
        item_names = [x.get_numbered_name(1, self.caller)[0] for x in obj.contents]
        for item in obj.contents:
            # assume a lootable object doesn't contain exits or other crazy objects
            item.move_to(caller.location, quiet=True)

        caller.location.msg_contents("%s loots %s and spills %s onto the floor." % (
            caller, obj, list_to_string(item_names)
        ))


class CmdInventory(AltCommand):
    """
    view inventory

    Usage:
      inventory
      inv

    Shows your inventory.
    """
    key = "inventory"
    aliases = ["inv", "i"]
    help_category = "General"
    locks = "cmd:all()"
    arg_regex = r"$"

    def func(self):
        """check inventory"""
        items = self.caller.contents
        if not items:
            string = "You are not carrying anything."
        else:
            table = self.styled_table(border="header")
            for item in items:
                table.add_row(
                    "(equipped)" if self.caller.is_equipped_with(item) else "",
                    "|C%s|n" % item.name,
                    item.db.desc_short or item.db.desc or "",
                )
            string = "|wYou are carrying:\n%s" % table
        self.caller.msg(string)


class CmdHints(AltCommand):
    """
    toggle hint messages

    Usage:
        hints

    Toggles hint messages on or off. This is especially useful if you are not
    using the default webclient and can't shuffle the hint messages to a
    separate window. Your character will remember your hint settings.
    """

    key = "hints"
    aliases = ["hint"]
    help_category = "System"

    def func(self):
        if self.caller.db.hints_enabled:
            self.caller.db.hints_enabled = False
            self.caller.msg("|530Hints|n are now |Cdisabled|n.")
            self.caller.msg(("Goodbye.", {"type": "hints"}))
        else:
            self.caller.db.hints_enabled = True
            self.caller.msg("|530Hints|n are now |cenabled|n.")
            self.caller.msg(("Welcome back!", {"type": "hints"}))
