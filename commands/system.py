from django.conf import settings
from evennia.server.sessionhandler import SESSIONS
from evennia.utils.utils import class_from_module

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)


# This command was copied here from evennia.commands.default.system.CmdReload() because we want
# "reload" to do something else.
class CmdRestart(COMMAND_DEFAULT_CLASS):
    """
    restart the server

    Usage:
      restart [reason]

    This restarts the server. The Portal is not
    affected. Non-persistent scripts will survive a restart (use
    reset to purge) and at_reload() hooks will be called.
    """
    key = "restart"
    aliases = ['sysreload']
    locks = "cmd:perm(reload) or perm(Developer)"
    help_category = "System"

    def func(self):
        """
        Restart the system.
        """
        reason = ""
        if self.args:
            reason = "(Reason: %s) " % self.args.rstrip(".")
        SESSIONS.announce_all(" Server restart initiated %s..." % reason)
        SESSIONS.portal_restart_server()