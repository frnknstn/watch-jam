import random

from evennia.utils.evtable import EvTable

import world.alternity
import world.skills
from commands.command import AltCommand
from world.dice import check_probability
from world.utils import rg_grad


# my custom commands
class CmdAbilities(AltCommand):
    """List a character's ability scores

    Usage:
      abilities
    """
    key = "abilities"
    aliases = ["abi"]
    lock = "cmd:all()"
    help_category = "Character"

    def func(self):
        char = self.caller
        output = "Str: %d Dex: %d Con: %d Int: %d Wil: %d Per: %d" % (
            # abilities
            char.db.strength,
            char.db.dexterity,
            char.db.constitution,
            char.db.intelligence,
            char.db.will,
            char.db.personality,
        )

        self.caller.msg(output)


class CmdCharacterSheet(AltCommand):
    """Show your character sheet

    Usage:
        character
        char
        character sheet

    Displays complete information about your character. If you want a smaller
    section of info, try the commands |xabi|n, |xhealth|n, or |xskills|n
    |C(available soon as DLC)|n.
    
    Since the text->web formatting is slightly buggy, I sometimes use "\xa0" 
    to get a &nbsp; non-breaking space element in the output. 
    """
    key = "character sheet"
    aliases = ["char", "character"]
    help_category = "Character"

    def func(self):
        char = self.caller  # type: typeclasses.characters.Character
        sheet = []

        # header
        sheet.append("""\
╒══════════════════════════════════════════════════════════════════════
│  |CWatch|n+|530Jam|w 2 - |CCharacter Sheet|n
├──────────────────────────────────────────────────────────────────────
""")
        sheet.append("│ |CName|n: |530%-19s|n\xa0|CPassword:|n %-11s |CGame Master|n: Frnknstn\n" % (
            char.key, random.choice(["Hunter2", "Blink182", "dickbutt", "fiduciary", "buttslol", "********"])
        ))
        sheet.append("│ |CProfession|n: |530%-13s|n\xa0(not ur actual password just a lovely joke)\n" % char.db.profession)

        # abilities
        def abi(char, score):
            res = char.calc_resistance_modifier(score)
            if res == 1:
                s = "[|530"
            elif res < 1:
                s = "[|Y"
            else:
                s = "[|y"
            return s + "%2d|n%%]" % (score*5)

        sheet.append("""│
│ |530ABILITY       Score   Resistance|n
│ |CStrength|n      %s    %+2d
│ |CDexterity|n     %s    %+2d
│ |CConstitution|n  %s    %+2d
│ |CIntelligence|n  %s    %+2d
│ |CWill|n          %s    %+2d
│ |CPersonality|n   %s    %+2d
""" % (     abi(char, char.db.strength), char.strength_res,
            abi(char, char.db.dexterity), char.dexterity_res,
            abi(char, char.db.constitution), char.constitution_res,
            abi(char, char.db.intelligence), char.intelligence_res,
            abi(char, char.db.will), char.will_res,
            abi(char, char.db.personality), char.personality_res,
        ))

        # actions
        init = char.action_check_score
        init_step = char.action_penalty
        sheet.append("""│
│ |CActions|n: |530%d|n per turn   |CStart Phase:|n [|C1|n]%d%%  [|C2|n]%d%%  [|C3|n]%d%%  [|C4|n]%d%%
""" %(
            char.action_count,
            check_probability(init // 4, init_step),
            check_probability(init // 2, init_step),
            check_probability(init, init_step),
            100
        ))

        # health
        sheet.append("""│
│ |CHealth|n: |xstun/wound/fatigue = unconscious   mortal = dead|n
│ %s
│ %s
""" % durability_string_lines(char))

        # skills
        #                        tree[abi_name][broad_name][spec_name] = level
        skill_tree = {}  # type: dict[str,     dict[str,   dict[str,     int]]]
        skill_tree.update((key, {}) for key in world.alternity.ABILITIES)
        raw_skills = dict(char.db.skills)

        for abi in world.alternity.ABILITIES:
            skill_tree[abi] = {}

        broad_name_to_spec_name_and_level = {}
        for skill_name, level in raw_skills.items():
            skill = world.skills.SKILL_LIST[skill_name]
            if skill.broad:
                # put our broad skills into the tree
                skill_tree[skill.ability][skill.name] = None
            else:
                # spec skills sorted into buckets based on broad skill name
                specs = broad_name_to_spec_name_and_level.get(skill.parent.name, {})
                specs[skill_name] = level
                broad_name_to_spec_name_and_level[skill.parent.name] = specs

        # put our spec skill names and levels into the tree
        for abi_name, broad_bucket in skill_tree.items():
            for broad_name in broad_bucket.keys():
                broad_bucket[broad_name] = broad_name_to_spec_name_and_level.get(broad_name, {})

        # sort and list our skills
        sheet.append("│\n")
        sheet.append("│ |530Skills|n:\n")

        def skill_lines_for(abi_name):
            lines = []
            broad_bucket = skill_tree[abi_name]
            if len(broad_bucket) == 0:
                return []
            lines.append("|c%s|n" % abi_name.title())
            for broad_name in sorted(broad_bucket.keys()):
                lines.append("\xa0|C%s|n" % broad_name)
                spec_bucket = broad_bucket[broad_name]
                for spec_name in sorted(spec_bucket.keys()):
                    lines.append("\xa0\xa0%s = |530%d|n" % (spec_name, spec_bucket[spec_name]))
            return lines

        # split skills into nice 3-col table
        table = EvTable(
            table=(skill_lines_for("strength") + skill_lines_for("dexterity"),
                   skill_lines_for("constitution") + skill_lines_for("intelligence"),
                   skill_lines_for("will") + skill_lines_for("personality")),
            border=None, border_width=0, border_left_char='\xa0', border_left=1,
            pad_width=0, pad_right=1
        )
        sheet.extend('│' + str(line) + '\n' for line in table.get())

        # status effects
        def status_details(status):
            if status.duration is not None:
                return "|c%s|n (|C%d|n)" % (status.description_short, status.duration)
            else:
                return "|c%s|n" % (status.description_short,)

        if char.get_all_status_effects():
            sheet.append("│\n")
            sheet.append("│ |530Status|n: ")
            sheet.append(", ".join(status_details(x) for x in char.get_all_status_effects()) + "\n")

        # footer
        sheet.append("""\
└──────────────────────────────────────────────────────────────────────""")

        char.msg("".join(sheet))


def durability_string_lines(char):
    """tuple of string lines with the formatted durability strings

    :type char: typeclasses.chararacters.Character
    :rtype: tuple[str]
    """
    # TODO: WEBCLIENT BUG:
    # char.msg("Fatigue: |050 8|n/|g8 |n")
    # char.msg("Mortal:  |050 8|n/|g8 |n")
    # These should be aligned but the we client swallows one of the spaces on the Mortal line

    w1, w2 = char.db.wound_post_first_aid, char.db.wound_post_treatment
    if w1 > w2:
        w1, w2 = w2, w1

    stun_str = health_bar(char.db.stun, char.db.stun_max)
    wound_str = health_bar_adv(char.db.wound, w1, w2, char.db.wound_max)
    mortal_str = health_bar(char.db.mortal, char.db.mortal_max)
    fatigue_str = health_bar(char.db.fatigue, char.db.fatigue_max)

    stun_str_len = len(stun_str)
    wound_str_len = len(health_bar(char.db.wound, char.db.wound_max))
    bar_width = max(stun_str_len, wound_str_len)

    return (
        " Stun: %s%s   Mortal: %s" % (stun_str, " "*(bar_width - stun_str_len), mortal_str),
        "Wound: %s%s  Fatigue: %s" % (wound_str, " "*(bar_width - wound_str_len), fatigue_str),
    )


def health_bar(h: int, h_max: int):
    """Draw a colored health bar"""
    return rg_grad(h / h_max) + "%2d|n/|g%-2d|n |G" % (h, h_max) + "▣"*h + "|r" + "▢" * (h_max - h) + "|n"


def health_bar_adv(h: int, r1: int, r2: int, h_max: int):
    """Draw a colored health bar with extra division in the red zone.

    r1 and r2 are the extra divisions red colors.

    e.g. If G is green, r is red, w1 is R and w2 is X, health_bar(2, 8, 4, 6) will give:
    GGrrRRXX
    """
    return rg_grad(h / h_max) + "%2d|n/|g%-2d|n |G" % (h, h_max) + \
        "▣" * h + \
        "|r" + "▢" * (r1 - h) + \
        "|R" + "▢" * (r2 - r1) + \
        "|R" + "▣" * (h_max - r2) + \
        "|n"


class CmdDurability(AltCommand):
    """Show your current durability / health

    Usage:
        durability
        health
        hp

    Displays your character's durability (health) status. For a complete
    character sheet instead, try the commands |xchar|n.
    """
    key = "durability"
    aliases = ["health", "hp"]
    help_category = "Character"

    def func(self):
        char = self.caller
        char.msg("\n".join(durability_string_lines(char)))

class CmdStatus(AltCommand):
    """Show your current status effects

    Usage:
        status

    Show the details, descriptions and durations for your current status
    effects.
    """
    key = "status"
    help_category = "Character"

    def func(self):
        char = self.caller

        if not char.get_all_status_effects():
            char.msg("You have no active status effects.")
            return

        lines = []

        for status in char.get_all_status_effects():
            if status.duration is not None:
                lines.append("|c%s|n (|C%d ticks|n) - %s" % (
                    status.description, status.duration, status.details))
            else:
                lines.append("|c%s|n - %s" % (
                    status.description, status.details))

        char.msg("\n".join(lines))

