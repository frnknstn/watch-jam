from evennia import DefaultCharacter

from commands.command import AltCommand
from commands.combat import CombatCommand

import world.alternity

class CmdRepair(AltCommand):
    """
    Attempt the repair of an object

    Usage:
        repair <target>

    Attempt to repair something that is broken. You may need to perform
    this action several times to complete the repairs.
    """

    key = "repair"
    lock = "cmd:all()"
    help_category = "skills"

    def func(self):
        args = self.args.strip()
        caller = self.caller

        if args == "":
            self.caller.msg("Usage: %s <object>" % self.cmdstring)
            return

        obj = caller.search(args)

        if obj is None:
            return

        required_repairs = obj.db.required_repairs
        if required_repairs is None or required_repairs <= 0:
            self.caller.msg("The %s does not need repairs." % obj.key)
            return

        self.caller.msg("You attempt to repair %s." % obj.key)
        caller.location.msg_contents("%s attempts to repair %s." %
                                        (caller.name,
                                         obj.name),
                                     exclude=caller)

        obj.attempt_repair(caller)


class CmdHeal(CombatCommand):
    """
    Attempt to heal a character via first aid or treatment

    Usage:
      heal <target>

    You cannot use this skill without either the |CFirst Aid|n skill or
    the |CTreatment|n skill.

    Attempt to heal a character using the your medical skills and a
    medical kit of some descriptions. The available treatments and
    potential outcomes depend on which medical skills you have. You will
    automatically perform the most urgent treatment first, in this order:

      * Stabilise a dying patient
      * Return a knocked out patient to consciousness
      * Provide |CFirst Aid|n for a patient's wounds
      * Provide |CTreatment|n for a patient's wounds
      * Heal some stun damage
    """

    key = "heal"
    aliases = ["treat", "cure"]
    lock = "cmd:all()"
    help_category = "skills"

    def func(self):
        caller = self.caller

        # validate our args
        if not self.args:
            caller.msg("Usage: %s <character>" % self.cmdstring)
            return

        target = caller.search(self.args, candidates=caller.location.contents)
        if not target:
            # search will alert the user why it couldn't find an item
            return

        if not isinstance(target, DefaultCharacter) or target.db.species != "human":
            # TODO: Speciest
            caller.msg("You can only %s living creatures." % self.cmdstring)
            return

        if not caller.has_item_by_key("first aid kit") and not caller.has_item_by_key("trauma pack"):
            caller.msg("You can't %s, you don't have a medical kit." % self.cmdstring)
            return

        if "Treatment" not in caller.db.skills and "First Aid" not in caller.db.skills:
            caller.msg("You can't %s, you are not trained in |CTreatment|n or |CFirst Aid|n." % self.cmdstring)
            return

        # check if we are in combat
        if caller.is_in_combat():
            arg = target
            description = "%s %s" % (self.key, str(arg))
            self.queue_action(arg, description)
        else:
            world.alternity.do_heal(caller, target)

    @staticmethod
    def combat_func(character, arg):
        # check the target is still valid
        if not arg or arg.location != character.location:
            character.msg("Your target is gone.")
            character.clear_combat_action()
            return

        world.alternity.do_heal(character, arg)

